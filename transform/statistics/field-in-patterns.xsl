<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:uml="http://www.omg.org/spec/UML/20110701" 
    xmlns:xmi="http://www.omg.org/spec/XMI/20110701" xmlns:ddifunc="ddi:functions"
    xmlns:xhtml="http://www.w3.org/1999/xhtml" exclude-result-prefixes="ddifunc uml xmi"
    version="2.0">
	
	<xsl:variable name="stylesheetVersion">1.0.0</xsl:variable>
    <xsl:output method="xml" indent="yes"/>

	<xsl:template match="xmi:XMI">
        <patterns>
            <xsl:comment>
				<xsl:text>This file was created by field-fillings version </xsl:text>
				<xsl:value-of select="$stylesheetVersion"/>
			</xsl:comment>
			<xsl:for-each select="//packagedElement[@xmi:type='uml:Package' and contains(@name, 'Pattern')]">
				<pattern>
					<xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
					<xsl:apply-templates select="packagedElement[@xmi:type='uml:Class']" mode="containedFields"/>
				</pattern>
			</xsl:for-each>
		</patterns>
	</xsl:template>
	
	<xsl:template match="packagedElement" mode="containedFields">
		<class>
			<xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
			<xsl:attribute name="superclass"><xsl:value-of select="generalization/@general"/></xsl:attribute>
			<xsl:for-each select="ownedAttribute[not(@association)]">
				<property>
					<xsl:attribute name="type">
						<xsl:choose>
							<xsl:when test="type/@xmi:idref">
								<xsl:value-of select="type/@xmi:idref"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:variable name="aType">
									<xsl:choose>
										<xsl:when test="contains(type/@href,'#')">
											<xsl:value-of select="substring-after(type/@href, '#')"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="type/@href"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>
								<xsl:value-of select="$aType"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:value-of select="@name"/>
				</property>
			</xsl:for-each>
			<xsl:for-each select="ownedAttribute[@association]">
				<association>
					<xsl:attribute name="type"><xsl:value-of select="type/@xmi:idref"/></xsl:attribute>
                    <xsl:variable name="associationID" select="@association"/>
                    <xsl:value-of select="//packagedElement[@xmi:id=$associationID]/@name"/>
				</association>
			</xsl:for-each>
		</class>
	</xsl:template>
	
</xsl:stylesheet>