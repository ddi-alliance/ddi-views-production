<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:uml="http://www.omg.org/spec/UML/20110701" 
    xmlns:xmi="http://www.omg.org/spec/XMI/20110701" xmlns:ddifunc="ddi:functions"
    xmlns:xhtml="http://www.w3.org/1999/xhtml" exclude-result-prefixes="ddifunc uml xmi"
    version="2.0">
	
	<xsl:variable name="stylesheetVersion">1.0.0</xsl:variable>
    <xsl:output method="text" indent="no"/>

	<xsl:template match="xmi:XMI">
				<xsl:text>This file was created by field-in-patterns version </xsl:text>
				<xsl:value-of select="$stylesheetVersion"/>
				<xsl:text>
</xsl:text>
			<xsl:for-each select="//packagedElement[@xmi:type='uml:Package' and contains(@name, 'Pattern')]">
				<xsl:text>Pattern: </xsl:text>
					<xsl:value-of select="@name"/>
				<xsl:text>
</xsl:text>
					<xsl:apply-templates select="packagedElement[@xmi:type='uml:Class']" mode="containedFields"/>
			</xsl:for-each>
	</xsl:template>
	
	<xsl:template match="packagedElement" mode="containedFields">
		<xsl:text>Class: </xsl:text>
		<xsl:value-of select="@name"/>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="generalization/@general"/>
		<xsl:text>,
</xsl:text>
		<xsl:for-each select="ownedAttribute[not(@association)]">
			<xsl:text> ,Property,</xsl:text>
			<xsl:value-of select="@name"/>
			<xsl:text>,</xsl:text>
			<xsl:choose>
				<xsl:when test="type/@xmi:idref">
					<xsl:value-of select="type/@xmi:idref"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:variable name="aType">
						<xsl:choose>
							<xsl:when test="contains(type/@href,'#')">
								<xsl:value-of select="substring-after(type/@href, '#')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="type/@href"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:value-of select="$aType"/>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:text>,
</xsl:text>
		</xsl:for-each>
		<xsl:for-each select="ownedAttribute[@association]">
			<xsl:text> ,Association,</xsl:text>
			<xsl:variable name="associationID" select="@association"/>
			<xsl:value-of select="//packagedElement[@xmi:id=$associationID]/@name"/>
			<xsl:text>,</xsl:text>
			<xsl:value-of select="type/@xmi:idref"/>
			<xsl:text>,
</xsl:text>
		</xsl:for-each>
	</xsl:template>
	
</xsl:stylesheet>