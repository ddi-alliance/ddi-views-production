<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:uml="http://www.omg.org/spec/UML/20110701" 
    xmlns:xmi="http://www.omg.org/spec/XMI/20110701" xmlns:ddifunc="ddi:functions"
    xmlns:xhtml="http://www.w3.org/1999/xhtml" exclude-result-prefixes="ddifunc uml xmi"
    version="2.0">
    
    <xsl:variable name="stylesheetVersion">1.0.0</xsl:variable>
    <xsl:output method="text" indent="no"/>
    
    <xsl:template match="xmi:XMI">
        <xsl:text>This file was created by inheritance.xsl version </xsl:text>
        <xsl:value-of select="$stylesheetVersion"/>
        <xsl:text>
</xsl:text>
        <xsl:text>Package,Class,isPatternClass,isAbstract,sub-classes/realizations
</xsl:text>
        <xsl:apply-templates select="//packagedElement[@xmi:type='uml:Class']" mode="class"/>
    </xsl:template>
    
    <xsl:template match="packagedElement" mode="class">
        <xsl:value-of select="../@name"/>
        <xsl:text>,</xsl:text>
        <xsl:value-of select="@name"/>
        <xsl:text>,</xsl:text>
        <xsl:choose>
            <xsl:when test="contains(../@name,'Pattern')">
                <xsl:text>true</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>false</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:text>,</xsl:text>
        <xsl:choose>
            <xsl:when test="@isAbstract='true'">
                <xsl:text>true</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>false</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:text>,</xsl:text>
        <xsl:choose>
            <xsl:when test="contains(../@name,'Pattern')">
                <xsl:variable name="myId" select="@xmi:id"/>
                <xsl:apply-templates select="//packagedElement[ownedAttribute[type/@xmi:idref=$myId and contains(@xmi:id,'_realizes_')]]" mode="sub"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="myId" select="@xmi:id"/>
                <xsl:apply-templates select="//packagedElement[generalization/@general=$myId]" mode="sub"/>
            </xsl:otherwise>
        </xsl:choose>
            <xsl:text>
</xsl:text>
    </xsl:template>
    
    <xsl:template match="packagedElement" mode="sub">
        <xsl:param name="pre"/>
        <xsl:param name="post"/>
        <xsl:value-of select="$pre"/>
        <xsl:value-of select="@name"/>
        <xsl:value-of select="$post"/>
        <xsl:text>;</xsl:text>
        <xsl:variable name="myId" select="@xmi:id"/>
        <xsl:apply-templates select="//packagedElement[generalization/@general=$myId]" mode="sub">
            <xsl:with-param name="pre"><xsl:value-of select="$pre"/><xsl:text>(</xsl:text></xsl:with-param>
            <xsl:with-param name="post"><xsl:value-of select="$post"/><xsl:text>)</xsl:text></xsl:with-param>
        </xsl:apply-templates>
    </xsl:template>
    
</xsl:stylesheet>
