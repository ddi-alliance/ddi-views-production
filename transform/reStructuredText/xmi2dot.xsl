<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:uml="http://www.omg.org/spec/UML/20110701" 
    xmlns:xmi="http://www.omg.org/spec/XMI/20110701" xmlns:ddifunc="ddi:functions"
    xmlns:xhtml="http://www.w3.org/1999/xhtml" exclude-result-prefixes="ddifunc uml xmi"
    version="2.0">
    
    <xsl:template match="packagedElement[@xmi:type='uml:Package']" mode="dot">
        <xsl:param name="path"/>
        <Graph>
            <xsl:attribute name="name">
                <xsl:value-of select="name"/>
            </xsl:attribute>
        </Graph>
        <xsl:result-document href="{$path}/images/graph/{name}/{name}.dot">
            <xsl:text>digraph G {
fontname = "Bitstream Vera Sans"
fontsize = 8

overlap=false;

compound=true;    

node [
  fontname = "Bitstream Vera Sans"
  fontsize = 8
  shape = "record"
]

edge [
  fontname = "Bitstream Vera Sans"
  fontsize = 8
  mode="ipsep"            
]


</xsl:text>
            <!-- TODO: Views from canonical XMI -->
            <!--
            <xsl:choose>
                <xsl:when test="../@xmi:id='ddi4_views'">
                    <xsl:variable name="viewID" select="@xmi:id"/>
                    <xsl:for-each select="//diagram[model/@package=$viewID]/elements/element">
                        <xsl:variable name="classId" select="@subject"/>
                        <xsl:apply-templates select="//packagedElement[@name=$classId]" mode="dotClass"/>
                    </xsl:for-each>
                    <xsl:variable name="references">
                        <xsl:for-each select="//diagram[model/@package=$viewID]/elements/element">
                            <xsl:variable name="classId" select="@subject"/>
                            <xsl:apply-templates select="//packagedElement[@name=$classId]" mode="collectReferences"/>
                        </xsl:for-each>
                    </xsl:variable>
                    <xsl:variable name="containedClasses">
                        <xsl:for-each select="//diagram[model/@package=$viewID]/elements/element">
                            <xsl:text>_</xsl:text>
                            <xsl:value-of select="@subject"/>
                            <xsl:text>_</xsl:text>
                        </xsl:for-each>
                    </xsl:variable>
                    <xsl:apply-templates select="//packagedElement[@xmi:id='ddi4_model']/packagedElement[@xmi:type='uml:Package']" mode="subgraph">
                        <xsl:with-param name="references" select="$references"/>
                        <xsl:with-param name="containedClasses" select="$containedClasses"/>
                    </xsl:apply-templates>
                    <xsl:for-each select="//diagram[model/@package=$viewID]/elements/element">
                        <xsl:variable name="classId" select="@subject"/>
                        <xsl:apply-templates select="//packagedElement[@name=$classId]" mode="dotClassConnections"/>
                    </xsl:for-each>
                </xsl:when>
                <xsl:otherwise>-->
                    <xsl:variable name="pId" select="@xmi:id"/>
                    <xsl:apply-templates select="packagedElement[@xmi:type='uml:Class']" mode="dotClass"/>
                    <xsl:variable name="references">
                        <xsl:apply-templates select="packagedElement[@xmi:type='uml:Class']" mode="collectReferences"/>
                    </xsl:variable>
                    <xsl:apply-templates select="//packagedElement[@xmi:id='ddi4_model']/packagedElement[@xmi:type='uml:Package' and @xmi:id != $pId]" mode="subgraph">
                        <xsl:with-param name="references" select="$references"/>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="packagedElement[@xmi:type='uml:Class']" mode="dotClassConnections"/>
<!--                </xsl:otherwise>
            </xsl:choose>-->
            <xsl:text>}</xsl:text>
        </xsl:result-document>
    </xsl:template>
    
    <xsl:template match="packagedElement[@xmi:type='uml:Class']" mode="dot">
        <xsl:param name="path"/>
        <Graph>
            <xsl:attribute name="name">
                <xsl:value-of select="name"/>
            </xsl:attribute>
        </Graph>
        <xsl:result-document href="{$path}/images/graph/{../name}/{name}.dot">
            <xsl:text>digraph G {
fontname = "Bitstream Vera Sans"
fontsize = 8

overlap=false;

compound=true;    

node [
  fontname = "Bitstream Vera Sans"
  fontsize = 8
  shape = "record"
]

edge [
  fontname = "Bitstream Vera Sans"
  fontsize = 8
  mode="ipsep"            
]


</xsl:text>
            <xsl:apply-templates select="." mode="dotClass"/>
            <xsl:variable name="references">
                <xsl:apply-templates select="." mode="collectReferences"/>
            </xsl:variable>
            <xsl:apply-templates select="//packagedElement[@xmi:id=$libIdPrefix]/packagedElement[@xmi:type='uml:Package']" mode="subgraph">
                <xsl:with-param name="references" select="$references"/>
                <xsl:with-param name="containedClasses"><xsl:value-of select="concat('_', @xmi:id, '_')"/></xsl:with-param>
            </xsl:apply-templates>
            <xsl:apply-templates select="." mode="dotClassConnections"/>
            <xsl:text>}</xsl:text>
        </xsl:result-document>
    </xsl:template>
    
    <xsl:template match="packagedElement" mode="collectReferences">
        <xsl:text>_</xsl:text>
        <xsl:value-of select="generalization/general/@xmi:idref"/>
        <xsl:text>_</xsl:text>
        <xsl:for-each select="ownedAttribute[association]">
            <xsl:text>_</xsl:text>
            <xsl:value-of select="type/@xmi:idref"/>
            <xsl:text>_</xsl:text>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="packagedElement" mode="subgraph">
        <xsl:param name="references"/>
        <xsl:param name="containedClasses"></xsl:param>
        <xsl:if test="packagedElement[contains($references, concat('_', @xmi:id, '_')) and not(contains($containedClasses, concat('_', @xmi:id, '_')))]">
            <xsl:text>subgraph </xsl:text>
            <xsl:value-of select="name"/>
            <xsl:text> {
label = "</xsl:text>
            <xsl:value-of select="name"/>
            <xsl:text>"    

  node [ color = "#0000ff" ]

</xsl:text>
            <xsl:apply-templates select="packagedElement[contains($references, concat('_', @xmi:id, '_')) and not(contains($containedClasses, concat('_', @xmi:id, '_')))]" mode="dotClass">
                <xsl:with-param name="prefix">DDI_</xsl:with-param>
                <xsl:with-param name="isSubgraph">true</xsl:with-param>
            </xsl:apply-templates>
            <xsl:text>}</xsl:text>
            <xsl:call-template name="linefeed"/>
            <xsl:call-template name="linefeed"/>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="packagedElement[@xmi:type='uml:Class']" mode="dotClass">
        <xsl:param name="prefix"><xsl:text></xsl:text></xsl:param>
        <xsl:param name="isSubgraph">false</xsl:param>
        <xsl:text>DDI4_</xsl:text>
        <xsl:value-of select="@xmi:id"/>
        <xsl:text> [</xsl:text>
        <xsl:call-template name="linefeed"/>
        <xsl:text>  label = "{&lt;id&gt;</xsl:text>
        <xsl:value-of select="$prefix"/>
        <xsl:value-of select="name"/>
        <xsl:if test="$prefix = ''">
            <xsl:text> |</xsl:text>
            <xsl:for-each select="ownedAttribute[not(association)]">
                <xsl:text> + </xsl:text>
                <xsl:value-of select="name"/>
                <xsl:text> : </xsl:text>
                <xsl:variable name="typeID" select="type/@xmi:idref"/>
                <xsl:value-of select="//packagedElement[@xmi:id=$typeID]/name"/>
                <xsl:text> \l</xsl:text>
            </xsl:for-each>
            <xsl:text> \l \l|</xsl:text>
            <xsl:for-each select="ownedAttribute[association]">
                <xsl:variable name="associationID" select="association/@xmi:idref"/>
                <xsl:variable name="typeID" select="type/@xmi:idref"/>
                <xsl:text> &lt;</xsl:text>
                <xsl:value-of select="//packagedElement[@xmi:id=$typeID]/name"/>
                <xsl:text>&gt; </xsl:text>
                <xsl:variable name="associationID" select="@association"/>
                <xsl:value-of select="//packagedElement[@xmi:id=$associationID]/name"/>
                <xsl:if test="not(position()=last())">
                    <xsl:text> \l|</xsl:text>
                </xsl:if>
            </xsl:for-each>
            <xsl:text> \l \l</xsl:text>
        </xsl:if>
        <xsl:text>}"</xsl:text>
        <xsl:call-template name="linefeed"/>
        <xsl:call-template name="linefeed"/>
        <xsl:text>  tooltip = "</xsl:text>
        <xsl:value-of select="../name"/>
        <xsl:text>:</xsl:text>
        <xsl:value-of select="name"/>
        <xsl:text>"</xsl:text>
        <xsl:call-template name="linefeed"/>
        <xsl:call-template name="linefeed"/>
        <xsl:choose>
            <xsl:when test="$isSubgraph='true'">
                <xsl:text>href = "</xsl:text>
                <xsl:if test="$prefix!= ''">
                    <xsl:text>../Package/</xsl:text>
                    <xsl:value-of select="../name"/>
                    <xsl:text>/</xsl:text>
                </xsl:if>
                <xsl:value-of select="name"/>
                <xsl:text>/index.html"</xsl:text>
            </xsl:when>
        </xsl:choose>
        <xsl:call-template name="linefeed"/>
        <xsl:text>]</xsl:text>
        <xsl:call-template name="linefeed"/>
        <xsl:call-template name="linefeed"/>
    </xsl:template>

    <xsl:template match="packagedElement[@xmi:type='uml:Class']" mode="dotClassConnections">
        <!-- 
DDI4_Relation:RelationsourceObject -> DDI4_Agent [ arrowhead=none labeldistance=1.9 taillabel="0..n" headlabel="1..1"  edgetooltip="sourceObject" fontcolor="black" color="#00000"];
DDI4_Universe -> DDI4_Universe:UniversehasSubuniverse [side=r arrowhead=diamond labeldistance=1.9 taillabel="0..n" headlabel="1..1"  edgetooltip="hasSubuniverse" fontcolor="black" color="#00000"];
arrowtail diamond odiamond
        -->
        <xsl:text>DDI4_</xsl:text>
        <xsl:value-of select="name"/>
        <xsl:text> -&gt; DDI4_</xsl:text>
        <xsl:variable name="generalID" select="generalization/general/@xmi:idref"/>
        <xsl:value-of select="//packagedElement[@xmi:id=$generalID]/name"/>
        <xsl:text> [arrowhead=onormal color="#000000"];</xsl:text>
        <xsl:call-template name="linefeed"/>
        <xsl:call-template name="linefeed"/>
        <xsl:for-each select="ownedAttribute[association]">
            <xsl:variable name="associationID" select="association/@xmi:idref"/>
            <xsl:text>  DDI4_</xsl:text>
            <xsl:value-of select="../name"/>
            <xsl:text>:</xsl:text>
            <xsl:value-of select="//packagedElement[@xmi:id=$associationID]/name"/>
            <xsl:text> -&gt; DDI4_</xsl:text>
            <xsl:variable name="typeID" select="type/@xmi:idref"/>
            <xsl:value-of select="//packagedElement[@xmi:id=$typeID]/name"/>
            <xsl:text> [ arrowhead=none </xsl:text>
            <xsl:choose>
                <xsl:when test="//packagedElement[@xmi:id=$associationID]/ownedEnd/lowerValue/value &gt;= 1"><xsl:text></xsl:text>arrowtail=diamond</xsl:when>
                <xsl:otherwise><xsl:text>arrowtail=odiamond</xsl:text></xsl:otherwise>
            </xsl:choose>
            <xsl:text> labeldistance=1.9 taillabel="</xsl:text>
            <xsl:if test="not(lowerValue/value)"><xsl:text>0</xsl:text></xsl:if>
            <xsl:value-of select="lowerValue/value"/>
            <xsl:text>..</xsl:text>
            <xsl:choose>
                <xsl:when test="not(upperValue/value)"><xsl:text>n</xsl:text></xsl:when>
                <xsl:when test="upperValue/value='-1'"><xsl:text>n</xsl:text></xsl:when>
                <xsl:otherwise><xsl:value-of select="upperValue/value"/></xsl:otherwise>
            </xsl:choose>
            <xsl:text>" headlabel="</xsl:text>
            <xsl:if test="not(//packagedElement[@xmi:id=$associationID]/ownedEnd/lowerValue/value)"><xsl:text>0</xsl:text></xsl:if>
            <xsl:value-of select="//packagedElement[@xmi:id=$associationID]/ownedEnd/lowerValue/value"/>
            <xsl:text>..</xsl:text>
            <xsl:choose>
                <xsl:when test="not(//packagedElement[@xmi:id=$associationID]/ownedEnd/upperValue/value)"><xsl:text>n</xsl:text></xsl:when>
                <xsl:when test="//packagedElement[@xmi:id=$associationID]/ownedEnd/upperValue/value='-1'"><xsl:text>n</xsl:text></xsl:when>
                <xsl:otherwise><xsl:value-of select="//packagedElement[@xmi:id=$associationID]/ownedEnd/upperValue/value"/></xsl:otherwise>
            </xsl:choose>
            <xsl:text>"</xsl:text>
            <xsl:text> edgetooltip="</xsl:text>
            <xsl:value-of select="//packagedElement[@xmi:id=$associationID]/name"/>
            <xsl:text>" fontcolor="black" color="#00000"];</xsl:text>
            <xsl:call-template name="linefeed"/>
            <xsl:call-template name="linefeed"/>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>