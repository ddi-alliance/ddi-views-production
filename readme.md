﻿# DDI Moving forward procuttion build (deprecated repository)

Historical repository for the model and documentation of DDI-Views, developed by [DDI moving forward](https://ddi-alliance.atlassian.net/wiki/x/t4AH)

The documentation of the model in this repository is automagically compiled and published on https://ddi-views.readthedocs.org

This repository contains:

 * XSLT transformations from XMI to XSD and RDFS/OWL
 * Documentation (restructured text used to procude html, epub and pdf)

## Repository structure
The repository contains three main folders

- transform, the XSLT transformation
- documentation, restructured text files
- buildsystem, build system currently ANT to transform XMI to RDFS/OWL and XSD and building the documentation

## Build configuration
The file build.properties contains the configuration of the build input and build enviroment.

The build begins by downloading a XMI file from [Lion the capturing platform for the DDI4 model](http://lion.ddialliance.org)
The default XMI file is of the whole of DDI4. To change this edit the build.properties file property _xmi.uri_ to point to a different XMI file. Eg.:
```
xmi.uri=http://lion.ddialliance.org/package/datacapture/xmi.xml
```
## Build structure
The build folder _buildsystem/ant_ will create the following structure for build delevirables:

 - tmp, folder containing downloaded content from [Lion](http://lion.ddialliance.org)
 - build, all the products of the build process
 - dist, distrubution folder containing single files for distribution

## Building
Currently the build system used is ANT to identify ant targets:
```
$ cd buildsystem/ant
$ ant -p
Main targets:

 build-annotated-owl      Build XSD file
 build-annotated-xsd      Build XSD file
 build-docs               Run Sphinx on RST files
 build-owl                Build OWL file
 build-psm-owl            Build platform specific model for OWL binding
 build-psm-xsd            Build platform specific model for XML binding
 build-xsd                Build XSD files for library and views
 build-doc-from-xmi       Creating reStructured text and dot files from XMI
 clean                    clean up
 create-readme            Creates readme in the build directory
 create-zip               Create dist zip-file
 fail-if-file-not-found   Check file exists, fail if not
 get-all                  Get all needed resources
 get-file                 Download file from specified location to specified location
 get-objects              Download dot-file (Graphviz)
 get-packagelist          Download Package list file
 get-packages             Download all packages listed in package file
 get-xmi                  Download XMI file
 init                     Setup the build
 reformat-sourcefile      Indent XML for specified file
 run-exec-with-cl         Run specified executable file, using specified command line
 run-jar-with-cl          Run specified jar file, using specified command line
 run-sphinx               Run Sphinx to build the documentation
 validate-library-schema  Validate owl XMI file
 validate-owl-xmi         Validate owl XMI file
 validate-schema-xmi      Validate schema XMI file
 validate-source-xmi      Validate source XMI file
 validate-xml             Validate XML file
 xslt-transform           Execute xslt transformation
Default target: init
```

To start of the build create the build folder structure:
```
$ ant init
```

Download [Lion](http://lion.ddialliance.org) content: 
To build the XSD's:
```
$ ant get-all
```

Build the DDI4 XSD's:
```
$ ant build-psm-xsd build-xsd build-annotated-xsd
```

To build the DDI4 RDFS/OWL:
```
$ ant build-psm-owl build-owl build-annotated-owl
```

To build the documentation:
```
$ ant build-docs
```

## System dependencies
Setting up the the build system for building XSD's and RDFS/OWL files a running ANT enviroment is needed.

* Apache ANT
  To run the build scripts

* Graphviz
  To generate svg and png from the dot-files

* Sphinx (requires Python)
  To generate html and pdf from the resturctured text documentation

  
### Linux

_ANT_, to install: 
```
$ sudo apt-get install ant
```

_Graphwiz_, to install:
```
$ sudo apt-get install graphwiz
```

_Python_ and _PIP_, _Sphinx_ and PDF extension to Sphinx, to install:
```
$ sudo apt-get install python python-pip
$ sudo pip install sphinx
$ sudo pip install rst2pdf
```

### Windows and OSX

_ANT_, get binaries from https://ant.apache.org

_Graphviz_, get binaries from http://www.graphviz.org

_Sphinx_, get binaries from http://sphinx-doc.org

### new production flow

_Docker_
currently 3 
_jenkins_
recommended plugins
Docker build step plugin
sudo docker run -d -p 8080:8080 -p 50000:50000 -v ~/GIT/ddi-views-production/buildsystem/docker/jenkins/jenkins_home:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock jenkins
