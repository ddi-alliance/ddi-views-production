<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:uml="http://www.omg.org/spec/UML/20110701" 
    xmlns:xmi="http://www.omg.org/spec/XMI/20110701" xmlns:ddifunc="ddi:functions"
    xmlns:xhtml="http://www.w3.org/1999/xhtml" exclude-result-prefixes="ddifunc uml xmi"
    version="2.0">
    
    <!-- imports -->
    <xsl:import href="../Util/reStructure.xsl"/>
    
    <!-- options -->
    <xsl:output method="text" indent="yes"/>
    
    <!-- params -->
    <xsl:param name="filepath" select="'file:///home/oliver/DDI/output/'"/>
    
    <xsl:variable name="stylesheetVersion">1.0.0</xsl:variable>

    <xsl:template match="xmi:XMI">
        <protocol>
            <xsl:comment>
				<xsl:text>This file was created by xmi2cogs version </xsl:text>
				<xsl:value-of select="$stylesheetVersion"/>
			</xsl:comment>
            <xsl:result-document href="{$filepath}Topics/index.txt">
                <xsl:for-each select="//packagedElement/packagedElement[@xmi:type='uml:Package']">
                    <xsl:if test="../@xmi:id='ddi4_views'"><xsl:text>*</xsl:text></xsl:if>
                    <xsl:value-of select="@name"/>
                    <xsl:call-template name="linefeed"/>
                </xsl:for-each>
            </xsl:result-document>
                
            <Packages>
                <xsl:apply-templates select="//packagedElement[@xmi:id='ddi4_model']/packagedElement[@xmi:type='uml:Package']" mode="package"/>
            </Packages>
            <Views>
                <xsl:apply-templates select="//packagedElement[@xmi:id='ddi4_views']/packagedElement[@xmi:type='uml:Package']" mode="package"/>
            </Views>
        </protocol>
    </xsl:template>
    
    <xsl:template match="packagedElement" mode="package">
        <Package>
            <xsl:value-of select="@name"/>
            <xsl:result-document href="{$filepath}Topics/{@name}/items.txt">
                <xsl:for-each select="packagedElement[@xmi:type='uml:Class' or @xmi:type='uml:Enumeration']">
                    <xsl:value-of select="@name"/>
                    <xsl:call-template name="linefeed"/>
                </xsl:for-each>
            </xsl:result-document>
            <xsl:result-document href="{$filepath}Topics/{@name}/readme.markdown">
                <xsl:value-of select="ownedComment/body"/>
            </xsl:result-document>
            <xsl:apply-templates select="packagedElement[@xmi:type='uml:Class' or @xmi:type='uml:Enumeration']" mode="packageClass"/>
        </Package>
    </xsl:template>
    
    <xsl:template match="packagedElement" mode="packageClass">
        <Class>
            <xsl:attribute name="name" select="@name"/>
        </Class>
        
        <xsl:variable name="section">
            <xsl:choose>
                <xsl:when test="../@name='ComplexDataTypes' or ../@name='EnumerationsRegExp' or ../@name='RegularExpressions' or ../@name='StructuredDataTypes'">CompositeTypes</xsl:when>
                <xsl:otherwise>ItemTypes</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
		
        <xsl:if test="@isAbstract='true'">
            <xsl:result-document href="{$filepath}{$section}/{@name}/Abstract">
                <xsl:text> </xsl:text>
            </xsl:result-document>
        </xsl:if>
        <xsl:choose>
            <xsl:when test="@xmi:type='uml:Enumeration'">
                <xsl:result-document href="{$filepath}{$section}/{@name}/entries.txt">
                    <xsl:for-each select="ownedLiteral">
                        <xsl:value-of select="@name"/>
                        <xsl:call-template name="linefeed"/>
                    </xsl:for-each>
                </xsl:result-document>
            </xsl:when>
            <xsl:otherwise>
                <xsl:result-document href="{$filepath}{$section}/{@name}/{@name}.csv">
                    <xsl:text>Name,DataType,MinCardinality,MaxCardinality,Description,Ordered,AllowSubtypes,MinLength,MaxLength,Enumeration,Pattern,MinInclusive,MinExclusive,MaxInclusive,MaxExclusive,DeprecatedNamespace,DeprecatedElementOrAttribute,DeprecatedChoiceGroup</xsl:text>
                    <xsl:call-template name="linefeed"/>
                    <xsl:apply-templates select="ownedAttribute"/>
                </xsl:result-document>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:result-document href="{$filepath}{$section}/{@name}/readme.markdown">
            <xsl:value-of select="ownedComment/body"/>
        </xsl:result-document>
        <xsl:if test="generalization/@general">
            <xsl:result-document href="{$filepath}{$section}/{@name}/Extends.{generalization/@general}">
                <xsl:text> </xsl:text>
            </xsl:result-document>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="ownedAttribute">
        <xsl:variable name="name" select="@name"/>
        <xsl:variable name="classname" select="../@name"/>
        <xsl:variable name="paid" select="@association"/>
        <xsl:variable name="idref" select="type/@xmi:idref"/>
        <xsl:variable name="href" select="type/@href"/>
        <xsl:variable name="xmitype" select="type/@xmi:type"/>
        <xsl:variable name="pa" select="//packagedElement[@xmi:id=$paid and @xmi:type='uml:Association']"/>
        
        <xsl:choose>
            <xsl:when test="@aggregation or @association">
                <xsl:value-of select="$pa/@name"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="@name"/>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:text>,</xsl:text>
        <xsl:choose>
            <xsl:when test="$idref!= ''">
                <xsl:value-of select="$idref"/>
            </xsl:when>
            <xsl:when test="$href!= ''">
                <xsl:value-of select="$href"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$xmitype"/>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:text>,</xsl:text>
        <xsl:choose>
            <xsl:when test="@aggregation or @association">
                <xsl:value-of select="$pa/ownedEnd/lowerValue/@value"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="lowerValue/@value"/>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:text>,</xsl:text>
        <xsl:choose>
            <xsl:when test="@aggregation or @association">
                <xsl:value-of select="$pa/ownedEnd/upperValue/@value"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="upperValue/@value"/>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:text>,"</xsl:text>
        <xsl:choose>
            <xsl:when test="@aggregation or @association">
                <!--<xsl:value-of select="replace($pa/ownedComment/body,'&#34;', '\\&#34;')"/>-->
                <xsl:value-of select="replace($pa/ownedComment/body,'&#34;', '&#34;&#34;')"/>
            </xsl:when>
            <xsl:otherwise>
                <!--<xsl:value-of select="replace(//*/element[@name=$classname]/attributes/attribute[@name=$name]/documentation/@value,'&#34;', '\\&#34;')"/>-->
                <xsl:value-of select="replace(//*/element[@name=$classname]/attributes/attribute[@name=$name]/documentation/@value,'&#34;', '&#34;&#34;')"/>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:text>",</xsl:text>
        <xsl:text>,</xsl:text>
        <xsl:text>,</xsl:text>
        <xsl:text>,</xsl:text>
        <xsl:text>,</xsl:text>
        <xsl:text>,</xsl:text>
        <xsl:text>,</xsl:text>
        <xsl:text>,</xsl:text>
        <xsl:text>,</xsl:text>
        <xsl:text>,</xsl:text>
        <xsl:text>,</xsl:text>
        <xsl:text>,</xsl:text>
        <xsl:text>,</xsl:text>
        <xsl:call-template name="linefeed"/>
    </xsl:template>
</xsl:stylesheet>
