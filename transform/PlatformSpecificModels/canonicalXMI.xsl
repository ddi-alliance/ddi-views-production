<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:uml="http://www.omg.org/spec/UML/20110701" 
    xmlns:xmi="http://www.omg.org/spec/XMI/20110701" xmlns:ddifunc="ddi:functions"
    exclude-result-prefixes="ddifunc"
    version="2.0">
    
    <!-- options -->
    <xsl:output method="xml" indent="yes"/>
	
	<xsl:template match="xmi:XMI">
		<xmi:XMI xmlns:uml="http://www.omg.org/spec/UML/20110701" xmlns:xmi="http://www.omg.org/spec/XMI/20110701">		<xsl:copy-of select="xmi:Documentation"/>
			<xsl:apply-templates select="uml:Model"/>
			<xsl:apply-templates select="xmi:Extension"/>
		</xmi:XMI>
	</xsl:template>
	
	<xsl:template match="uml:Model">
		<xsl:variable name="myId" select="@xmi:id"/>
		<uml:Model>
			<xsl:attribute name="xmi:id"><xsl:value-of select="$myId"/></xsl:attribute>
			<xsl:attribute name="xmi:uuid"><xsl:value-of select="generate-id()"/></xsl:attribute>
			<name><xsl:value-of select="@name"/></name>
			<xsl:apply-templates select="packagedElement[1]" mode="libraryToXMI">
				<xsl:with-param name="idPrefix">
					<xsl:value-of select="$myId"/>
					<xsl:text>-</xsl:text>
				</xsl:with-param>
			</xsl:apply-templates>
			<xsl:apply-templates select="packagedElement[2]" mode="viewsToXMI">
				<xsl:with-param name="idPrefix">
					<xsl:value-of select="$myId"/>
					<xsl:text>-</xsl:text>
				</xsl:with-param>
			</xsl:apply-templates>
			<xsl:copy-of select="packagedElement[2]"/>
		</uml:Model>
	</xsl:template>
	
	<xsl:template match="packagedElement" mode="libraryToXMI">
		<xsl:param name="idPrefix"/>
		<xsl:variable name="myId">
			<xsl:value-of select="idPrefix"/>
			<xsl:value-of select="@xmi:id"/>
		</xsl:variable>
		<packagedElement>
			<xsl:attribute name="xmi:id"><xsl:value-of select="$myId"/></xsl:attribute>
			<xsl:attribute name="xmi:uuid"><xsl:value-of select="generate-id()"/></xsl:attribute>
			<xsl:attribute name="xmi:type">uml:Package</xsl:attribute>
			<name><xsl:value-of select="@name"/></name>
			<xsl:apply-templates select="packagedElement" mode="packageToXMI">
				<xsl:with-param name="idPrefix">
					<xsl:value-of select="$myId"/>
					<xsl:text>-</xsl:text>
				</xsl:with-param>
			</xsl:apply-templates>
		</packagedElement>
	</xsl:template>
	
	<xsl:template match="packagedElement" mode="viewsToXMI">
		<xsl:param name="idPrefix"/>
		<xsl:variable name="myId">
			<xsl:value-of select="idPrefix"/>
			<xsl:value-of select="@xmi:id"/>
		</xsl:variable>
		<packagedElement>
			<xsl:attribute name="xmi:id"><xsl:value-of select="$myId"/></xsl:attribute>
			<xsl:attribute name="xmi:uuid"><xsl:value-of select="generate-id()"/></xsl:attribute>
			<xsl:attribute name="xmi:type">uml:Package</xsl:attribute>
			<name><xsl:value-of select="@name"/></name>
			<xsl:apply-templates select="packagedElement" mode="viewToXMI">
				<xsl:with-param name="idPrefix">
					<xsl:value-of select="$myId"/>
					<xsl:text>-</xsl:text>
				</xsl:with-param>
			</xsl:apply-templates>
		</packagedElement>
	</xsl:template>
	
	<xsl:template match="packagedElement" mode="viewToXMI">
		<xsl:param name="idPrefix"/>
		<xsl:variable name="myId">
			<xsl:value-of select="idPrefix"/>
			<xsl:value-of select="@xmi:id"/>
		</xsl:variable>
		<packagedElement>
			<xsl:attribute name="xmi:id"><xsl:value-of select="$myId"/></xsl:attribute>
			<xsl:attribute name="xmi:uuid"><xsl:value-of select="generate-id()"/></xsl:attribute>
			<xsl:attribute name="xmi:type">uml:Package</xsl:attribute>
			<name><xsl:value-of select="@name"/></name>
			<ownedComment>
				<xsl:attribute name="xmi:id"><xsl:value-of select="$myId"/>-ownedComment</xsl:attribute>
				<xsl:attribute name="xmi:uuid"><xsl:value-of select="generate-id()"/></xsl:attribute>
				<xsl:copy-of select="ownedComment/body"/>
				<annotatedElement>
					<xsl:attribute name="xmi:idref"><xsl:value-of select="$myId"/></xsl:attribute>
				</annotatedElement>
			</ownedComment>
			<xsl:variable name="viewId"><xsl:value-of select="@xmi:id"/></xsl:variable>
			<xsl:for-each select="//diagram[model/@owner=$viewId]/elements/element">
				<elementImport>
					<xsl:attribute name="xmi:id">
						<xsl:value-of select="$myId"/>
						<xsl:text>-elementImport</xsl:text>
						<xsl:value-of select="@seqno"/>
					</xsl:attribute>
					<xsl:attribute name="xmi:uuid"><xsl:value-of select="generate-id()"/></xsl:attribute>
					<xsl:attribute name="xmi:type">uml:ElementImport</xsl:attribute>
					<xsl:variable name="classId"><xsl:value-of select="@subject"/></xsl:variable>
					<importedElement>
						<xsl:attribute name="xmi:idref">
							<xsl:apply-templates select="//packagedElement[@xmi:id=$classId]" mode="createIdentifier"/>
						</xsl:attribute>
					</importedElement>
				</elementImport>
			</xsl:for-each>
		</packagedElement>
	</xsl:template>
	
	<xsl:template match="packagedElement" mode="packageToXMI">
		<xsl:param name="idPrefix"/>
		<xsl:variable name="myId">
			<xsl:value-of select="idPrefix"/>
			<xsl:value-of select="@xmi:id"/>
		</xsl:variable>
		<packagedElement>
			<xsl:attribute name="xmi:id"><xsl:value-of select="$myId"/></xsl:attribute>
			<xsl:attribute name="xmi:uuid"><xsl:value-of select="generate-id()"/></xsl:attribute>
			<xsl:attribute name="xmi:type">uml:Package</xsl:attribute>
			<name><xsl:value-of select="@name"/></name>
			<ownedComment>
				<xsl:attribute name="xmi:id"><xsl:value-of select="$myId"/>-ownedComment</xsl:attribute>
				<xsl:attribute name="xmi:uuid"><xsl:value-of select="generate-id()"/></xsl:attribute>
				<xsl:copy-of select="ownedComment/body"/>
				<annotatedElement>
					<xsl:attribute name="xmi:idref"><xsl:value-of select="$myId"/></xsl:attribute>
				</annotatedElement>
			</ownedComment>
			<xsl:apply-templates select="packagedElement" mode="decide">
				<xsl:sort select="@xmi:id"/>
				<xsl:with-param name="idPrefix">
					<xsl:value-of select="$myId"/>
					<xsl:text>-</xsl:text>
				</xsl:with-param>
			</xsl:apply-templates>
		</packagedElement>
	</xsl:template>
	
	<xsl:template match="packagedElement" mode="decide">
		<xsl:param name="idPrefix"/>
		<xsl:choose>
			<xsl:when test="@xmi:type='uml:Class'">
				<xsl:apply-templates select="." mode="classToXMI">
					<xsl:sort select="@xmi:id"/>
					<xsl:with-param name="idPrefix">
						<xsl:value-of select="$idPrefix"/>
					</xsl:with-param>
				</xsl:apply-templates>
			</xsl:when>
			<xsl:when test="@xmi:type='uml:Enumeration'">
				<xsl:apply-templates select="." mode="enumerationToXMI">
					<xsl:sort select="@xmi:id"/>
					<xsl:with-param name="idPrefix">
						<xsl:value-of select="$idPrefix"/>
					</xsl:with-param>
				</xsl:apply-templates>
			</xsl:when>
			<xsl:when test="@xmi:type='uml:DataType'">
				<xsl:apply-templates select="." mode="dataTypeToXMI">
					<xsl:sort select="@xmi:id"/>
					<xsl:with-param name="idPrefix">
						<xsl:value-of select="$idPrefix"/>
					</xsl:with-param>
				</xsl:apply-templates>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="." mode="associationToXMI">
					<xsl:sort select="@xmi:id"/>
					<xsl:with-param name="idPrefix">
						<xsl:value-of select="$idPrefix"/>
					</xsl:with-param>
				</xsl:apply-templates>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template match="packagedElement" mode="ClassToXMI">
		<xsl:param name="idPrefix"/>
		<xsl:variable name="myName" select="@name"/>
		<xsl:variable name="myId">
			<xsl:value-of select="idPrefix"/>
			<xsl:value-of select="@xmi:id"/>
		</xsl:variable>
		<packagedElement>
			<xsl:attribute name="xmi:id"><xsl:value-of select="$myId"/></xsl:attribute>
			<xsl:attribute name="xmi:uuid"><xsl:value-of select="generate-id()"/></xsl:attribute>
			<xsl:attribute name="xmi:type">uml:Package</xsl:attribute>
			<name><xsl:value-of select="@name"/></name>
			<ownedComment>
				<xsl:attribute name="xmi:id"><xsl:value-of select="$myId"/>-ownedComment</xsl:attribute>
				<xsl:attribute name="xmi:uuid"><xsl:value-of select="generate-id()"/></xsl:attribute>
				<xsl:copy-of select="ownedComment/body"/>
				<annotatedElement>
					<xsl:attribute name="xmi:idref"><xsl:value-of select="$myId"/></xsl:attribute>
				</annotatedElement>
			</ownedComment>
			<xsl:variable name="parentId"><xsl:value-of select="generalization/@general"/></xsl:variable>
			<xsl:if test="$parentId != ''">
				<generalization>
					<xsl:attribute name="xmi:id"><xsl:value-of select="$myId"/>-generalization</xsl:attribute>
					<xsl:attribute name="xmi:uuid"><xsl:value-of select="generate-id()"/></xsl:attribute>
					<xsl:attribute name="xmi:type">uml:Generalization</xsl:attribute>
						<general>
							<xsl:apply-templates select="//packagedElement[@xmi:id=$parentId]" mode="createIdentifier"/>
						</general>
						<general xmi:idref="testCase1-package1-Class1"/>
				</generalization>
			</xsl:if>
			<xsl:for-each select="ownedAttribute">
				<xsl:sort select="@name"/>
				<ownedAttribute>
					<xsl:attribute name="xmi:id">
						<xsl:value-of select="$myId"/>
						<xsl:text>-</xsl:text>
						<xsl:value-of select="@name"/>
					</xsl:attribute>
					<xsl:attribute name="xmi:uuid"><xsl:value-of select="generate-id()"/></xsl:attribute>
					<xsl:copy-of select="@xmi:type"/>
					<visibility>public</visibility>
					<name><xsl:value-of select="@name"/></name>
					<type>
						<xsl:choose>
							<xsl:when test="type/@xmi:idref='uml:PrimitiveType'">
								<xsl:attribute name="href"><xsl:value-of select="type/@href"/></xsl:attribute>
							</xsl:when>
							<xsl:when test="not(type/@xmi:idref) and not (type/@xmi:href)">
								<xsl:variable name="myType"><xsl:value-of select="type/@xmi:type"/></xsl:variable>
								<xsl:attribute name="xmi:idref">
									<xsl:apply-templates select="//packagedElement[@name=$myType]" mode="createIdentifier"/>
								</xsl:attribute>
							</xsl:when>
							<xsl:otherwise>
								<xsl:variable name="myType"><xsl:value-of select="type/@xmi:idref"/></xsl:variable>
								<xsl:attribute name="xmi:idref">
									<xsl:apply-templates select="//packagedElement[@name=$myType]" mode="createIdentifier"/>
								</xsl:attribute>
							</xsl:otherwise>
						</xsl:choose>
					</type>
					<ownedComment>
						<xsl:attribute name="xmi:id">
							<xsl:value-of select="$myId"/>
							<xsl:text>-</xsl:text>
							<xsl:value-of select="@name"/>
							<xsl:text>-ownedComment</xsl:text>
						</xsl:attribute>
						<xsl:attribute name="xmi:uuid"><xsl:value-of select="generate-id()"/></xsl:attribute>
						<xsl:copy-of select="ownedComment/body"/>
						<annotatedElement>
							<xsl:attribute name="xmi:idref">
							<xsl:value-of select="$myId"/>
							<xsl:text>-</xsl:text>
							<xsl:value-of select="@name"/>
							</xsl:attribute>
						</annotatedElement>
					</ownedComment>
					<xsl:if test="lowerValue">
						<lowerValue>
							<xsl:attribute name="xmi:id">
								<xsl:value-of select="$myId"/>
								<xsl:text>-</xsl:text>
								<xsl:value-of select="@name"/>
								<xsl:text>-lowerValue</xsl:text>
							</xsl:attribute>
							<xsl:attribute name="xmi:uuid"><xsl:value-of select="generate-id()"/></xsl:attribute>
							<value><xsl:value-of select="lowerValue/@value"/></value>
						</lowerValue>
					</xsl:if>
					<xsl:if test="upperValue">
						<upperValue>
							<xsl:attribute name="xmi:id">
								<xsl:value-of select="$myId"/>
								<xsl:text>-</xsl:text>
								<xsl:value-of select="@name"/>
								<xsl:text>-upperValue</xsl:text>
							</xsl:attribute>
							<xsl:attribute name="xmi:uuid"><xsl:value-of select="generate-id()"/></xsl:attribute>
							<value><xsl:value-of select="upperValue/@value"/></value>
						</upperValue>
					</xsl:if>
					<xsl:if test="@association">
						<xsl:variable name="myAssociation"><xsl:value-of select="@association"/></xsl:variable>
						<association xmi:idref="testCase1-package1-packagedElement-6">
							<xsl:attribute name="xmi:idref">
								<xsl:apply-templates select="//packagedElement[@xmi:id=$myAssociation]" mode="createIdentifier"/>
							</xsl:attribute>
						</association>
					</xsl:if>
				</ownedAttribute>
			</xsl:for-each>
			<xsl:if test="@isAbstract='true'">
				<isAbstract>true</isAbstract>
			</xsl:if>
		</packagedElement>
	</xsl:template>
	
	<xsl:template match="packagedElement" mode="dataTypeToXMI">
		<xsl:param name="idPrefix"/>
		<xsl:variable name="myId">
			<xsl:value-of select="idPrefix"/>
			<xsl:value-of select="@xmi:id"/>
		</xsl:variable>
		<packagedElement>
			<xsl:attribute name="xmi:id"><xsl:value-of select="$myId"/></xsl:attribute>
			<xsl:attribute name="xmi:uuid"><xsl:value-of select="generate-id()"/></xsl:attribute>
			<xsl:attribute name="xmi:type">uml:DataType</xsl:attribute>
			<name><xsl:value-of select="@name"/></name>
		</packagedElement>
	</xsl:template>

	<xsl:template match="packagedElement" mode="associationToXMI">
		<xsl:param name="idPrefix"/>
		<xsl:variable name="myId">
			<xsl:value-of select="idPrefix"/>
			<xsl:value-of select="@name"/>
			<xsl:text>-association</xsl:text>
		</xsl:variable>
		<packagedElement>
			<xsl:attribute name="xmi:id"><xsl:value-of select="$myId"/></xsl:attribute>
			<xsl:attribute name="xmi:uuid"><xsl:value-of select="generate-id()"/></xsl:attribute>
			<xsl:attribute name="xmi:type">uml:Association</xsl:attribute>
			<ownedComment>
				<xsl:attribute name="xmi:id"><xsl:value-of select="$myId"/>-ownedComment</xsl:attribute>
				<xsl:attribute name="xmi:uuid"><xsl:value-of select="generate-id()"/></xsl:attribute>
				<xsl:copy-of select="ownedComment/body"/>
				<annotatedElement>
					<xsl:attribute name="xmi:idref"><xsl:value-of select="$myId"/></xsl:attribute>
				</annotatedElement>
			</ownedComment>
			<xsl:for-each select="memberEnd">
				<memberEnd>
					<xsl:variable name="myRef"><xsl:value-of select="@xmi:idref"/></xsl:variable>
					<xsl:attribute name="xmi:idref">
						<xsl:apply-templates select="//*[@xmi:id=$myRef]" mode="createIdentifier"/>
					</xsl:attribute>
				</memberEnd>
			</xsl:for-each>
			<xsl:for-each select="ownedEnd">
				<xsl:variable name="myRef"><xsl:value-of select="@xmi:idref"/></xsl:variable>
				<ownedEnd>
					<xsl:attribute name="xmi:id">
						<xsl:value-of select="$myId"/>
						<xsl:text>-target</xsl:text>
					</xsl:attribute>
					<xsl:attribute name="xmi:uuid"><xsl:value-of select="generate-id()"/></xsl:attribute>
					<xsl:attribute name="xmi:type">uml:Association</xsl:attribute>
					<xsl:variable name="className"><xsl:value-of select="type/@xmi:idref"/></xsl:variable>
					<type>
						<xsl:attribute name="xmi:idref">
							<xsl:apply-templates select="//*[@xmi:id=$myRef]" mode="createIdentifier"/>
						</xsl:attribute>
					</type>
						<xsl:if test="lowerValue">
							<lowerValue>
								<xsl:attribute name="xmi:id">
									<xsl:value-of select="$myId"/>
									<xsl:text>-target-lowerValue</xsl:text>
								</xsl:attribute>
								<xsl:attribute name="xmi:uuid"><xsl:value-of select="generate-id()"/></xsl:attribute>
								<value><xsl:value-of select="lowerValue/@value"/></value>
							</lowerValue>
						</xsl:if>
						<xsl:if test="upperValue">
							<upperValue>
								<xsl:attribute name="xmi:id">
									<xsl:value-of select="$myId"/>
									<xsl:text>-target-lowerValue</xsl:text>
								</xsl:attribute>
								<xsl:attribute name="xmi:uuid"><xsl:value-of select="generate-id()"/></xsl:attribute>
								<value><xsl:value-of select="upperValue/@value"/></value>
							</upperValue>
						</xsl:if>
				</ownedEnd>
			</xsl:for-each>
		</packagedElement>
	</xsl:template>
		
	<xsl:template match="packagedElement" mode="enumerationToXMI">
		<xsl:param name="idPrefix"/>
		<xsl:variable name="myId">
			<xsl:value-of select="idPrefix"/>
			<xsl:value-of select="@xmi:id"/>
		</xsl:variable>
		<packagedElement>
			<xsl:attribute name="xmi:id"><xsl:value-of select="$myId"/></xsl:attribute>
			<xsl:attribute name="xmi:uuid"><xsl:value-of select="generate-id()"/></xsl:attribute>
			<xsl:attribute name="xmi:type">uml:Association</xsl:attribute>
			<ownedComment>
				<xsl:attribute name="xmi:id"><xsl:value-of select="$myId"/>-ownedComment</xsl:attribute>
				<xsl:attribute name="xmi:uuid"><xsl:value-of select="generate-id()"/></xsl:attribute>
				<xsl:copy-of select="ownedComment/body"/>
				<annotatedElement>
					<xsl:attribute name="xmi:idref"><xsl:value-of select="$myId"/></xsl:attribute>
				</annotatedElement>
			</ownedComment>
			<xsl:for-each select="ownedLiteral">
				<ownedLiteral>
					<xsl:attribute name="xmi:id">
						<xsl:value-of select="$myId"/>
						<xsl:text>-literal</xsl:text>
						<xsl:value-of select="position()"/>
					</xsl:attribute>
					<xsl:attribute name="xmi:uuid"><xsl:value-of select="generate-id()"/></xsl:attribute>
					<xsl:attribute name="xmi:type">uml:EnumerationLiteral</xsl:attribute>
					<name><xsl:value-of select="@name"/></name>
				</ownedLiteral>
			</xsl:for-each>
		</packagedElement>
	</xsl:template>
	
	<xsl:template match="*" mode="createIdentifier">
		<xsl:choose>
			<xsl:when test="name()='uml:Model'">
				<xsl:value-of select="@xmi:id"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select=".." mode="createIdentifier"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="@xmi:id"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	</xsl:stylesheet>
