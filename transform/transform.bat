echo off

set outdir=C:/ddi/output
set xmipath=C:/ddi/xmi.xml

java -jar saxon9he.jar -s:%xmipath% -xsl:PlatformSpecificModels/xmi2xmi4owl.xsl -o:%outdir%/psm4owl.xml
java -jar saxon9he.jar -s:%xmipath% -xsl:PlatformSpecificModels/xmi2xmi4xsd.xsl -o:%outdir%/ddi4psm.xsd.xmi
java -jar saxon9he.jar -s:%outdir%/ddi4psm.xsd.xmi -xsl:XMLSchema/xmi2xsd.xsl -o:%outdir%/result-ddi4-xsd.xml filepath=file:///%outdir%/xsd/
java -jar saxon9he.jar -s:%xmipath% -xsl:COGS/xmi2cogs.xsl -o:%outdir%/result-ddi4-cogs.xml filepath=file:///%outdir%/cogs/
java -jar saxon9he.jar -s:%xmipath% -xsl:reStructuredText/xmi2reStructured.xsl -o:%outdir%/result-ddi4-reStructured.xml filepath=file:///%outdir%/reStructured
java -jar saxon9he.jar -s:%xmipath% -xsl:statistics/field-fillings.xsl -o:%outdir%/statistics/field_fillings_in_lion.fods filepath=file:///%outdir%/statistics/fieldfillings/
java -jar saxon9he.jar -s:%xmipath% -xsl:statistics/field-in-patterns-csv.xsl -o:%outdir%/statistics/fields_in_patterns.csv
java -jar saxon9he.jar -s:%xmipath% -xsl:statistics/inheritance.xsl -o:%outdir%/statistics/inherritance.csv
java -jar saxon9he.jar -s:%xmipath% -xsl:statistics/list-all-properties-and-relationships.xsl -o:%outdir%/statistics/Properties_and_relationships.csv
java -jar saxon9he.jar -s:%xmipath% -xsl:statistics/list-orphans.xsl -o:%outdir%/statistics/orphans.txt
