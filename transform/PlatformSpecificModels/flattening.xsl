<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:uml="http://www.omg.org/spec/UML/20110701" 
    xmlns:xmi="http://www.omg.org/spec/XMI/20110701" xmlns:ddifunc="ddi:functions"
    exclude-result-prefixes="ddifunc"
    version="2.0">
    
	<xsl:template match="packagedElement" mode="absractClassToXMI">
		<xsl:variable name="myName" select="name"/>
		<packagedElement>
			<xsl:attribute name="xmi:id">
				<xsl:value-of select="$modelId"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="../../name"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="../name"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="name"/>
			</xsl:attribute>
			<xsl:attribute name="xmi:uuid">
				<xsl:value-of select="$uuidPrefix"/>
				<xsl:text>#</xsl:text>
				<xsl:value-of select="$modelId"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="../../name"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="../name"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="name"/>
			</xsl:attribute>
			<xsl:copy-of select="@xmi:type"/>
			<xsl:if test="generalization">
				<generalization>
					<xsl:attribute name="xmi:id">
						<xsl:value-of select="$modelId"/>
						<xsl:text>-</xsl:text>
						<xsl:value-of select="../../name"/>
						<xsl:text>-</xsl:text>
						<xsl:value-of select="../name"/>
						<xsl:text>-</xsl:text>
						<xsl:value-of select="name"/>
						<xsl:text>-generalization</xsl:text>
					</xsl:attribute>
					<xsl:attribute name="xmi:uuid">
						<xsl:value-of select="$uuidPrefix"/>
						<xsl:text>#</xsl:text>
						<xsl:value-of select="$modelId"/>
						<xsl:text>-</xsl:text>
						<xsl:value-of select="../../name"/>
						<xsl:text>-</xsl:text>
						<xsl:value-of select="../name"/>
						<xsl:text>-</xsl:text>
						<xsl:value-of select="name"/>
						<xsl:text>-generalization</xsl:text>
					</xsl:attribute>
					<xsl:copy-of select="generalization/@xmi:type"/>
					<general>
						<xsl:attribute name="xmi:idref">
							<xsl:attribute name="xmi:idref">
								<xsl:value-of select="$idPrefix"/>
								<xsl:value-of select="substring-after(generalization/general/@xmi:idref,$pimIdPrefix)"/>
							</xsl:attribute>
						</xsl:attribute>
					</general>
				</generalization>
			</xsl:if>
			<xsl:copy-of select="name"/>
			<xsl:copy-of select="isAbstract"/>
			<xsl:copy-of select="ownedComment"/>
		</packagedElement>
	</xsl:template>
	
	<xsl:template match="packagedElement" mode="classToXMI">
		<xsl:variable name="parentClass" select="generalization/general/@xmi:idref"/>
		<xsl:variable name="className" select="@xmi:id"/>
		<xsl:variable name="overridden">
			<xsl:apply-templates select="." mode="fillOverridden"/>
		</xsl:variable>
		<packagedElement>
			<xsl:attribute name="xmi:id">
				<xsl:value-of select="$modelId"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="../../name"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="../name"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="name"/>
			</xsl:attribute>
			<xsl:attribute name="xmi:uuid">
				<xsl:value-of select="$uuidPrefix"/>
				<xsl:text>#</xsl:text>
				<xsl:value-of select="$modelId"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="../../name"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="../name"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="name"/>
			</xsl:attribute>
			<xsl:copy-of select="@xmi:type"/>
			<xsl:if test="generalization">
				<generalization>
					<xsl:attribute name="xmi:id">
						<xsl:value-of select="$modelId"/>
						<xsl:text>-</xsl:text>
						<xsl:value-of select="../../name"/>
						<xsl:text>-</xsl:text>
						<xsl:value-of select="../name"/>
						<xsl:text>-</xsl:text>
						<xsl:value-of select="name"/>
						<xsl:text>-generalization</xsl:text>
					</xsl:attribute>
					<xsl:attribute name="xmi:uuid">
						<xsl:value-of select="$uuidPrefix"/>
						<xsl:text>#</xsl:text>
						<xsl:value-of select="$modelId"/>
						<xsl:text>-</xsl:text>
						<xsl:value-of select="../../name"/>
						<xsl:text>-</xsl:text>
						<xsl:value-of select="../name"/>
						<xsl:text>-</xsl:text>
						<xsl:value-of select="name"/>
						<xsl:text>-generalization</xsl:text>
					</xsl:attribute>
					<xsl:copy-of select="generalization/@xmi:type"/>
					<general>
						<xsl:attribute name="xmi:idref">
							<xsl:attribute name="xmi:idref">
								<xsl:value-of select="$idPrefix"/>
								<xsl:value-of select="substring-after(generalization/general/@xmi:idref,$pimIdPrefix)"/>
							</xsl:attribute>
						</xsl:attribute>
					</general>
				</generalization>
			</xsl:if>
			<xsl:apply-templates select="//packagedElement[@xmi:id=$parentClass]" mode="parentClassToXMI">
				<xsl:with-param name="className" select="$className"/>
				<xsl:with-param name="overridden" select="$overridden"/>
			</xsl:apply-templates>
			<xsl:copy-of select="name"/>
			<xsl:copy-of select="isAbstract"/>
			<xsl:for-each select="ownedAttribute">
				<xsl:variable name="associationID" select="association/@xmi:idref"/>
				<xsl:variable name="name">
					<xsl:choose>
						<xsl:when test="association"><xsl:value-of select="//packagedElement[@xmi:id=$associationID]/name"/></xsl:when>
						<xsl:otherwise><xsl:value-of select="name"/></xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:if test="$name!='realizes'">
					<xsl:apply-templates select=".">
						<xsl:with-param name="baseid">
							<xsl:value-of select="$idPrefix"/>
							<xsl:value-of select="../../name"/>
							<xsl:text>-</xsl:text>
							<xsl:value-of select="../name"/>
							<xsl:text>-</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="name"><xsl:value-of select="$name"/></xsl:with-param>
					</xsl:apply-templates>
				</xsl:if>
			</xsl:for-each>
			<xsl:copy-of select="ownedComment"/>
		</packagedElement>
		<xsl:for-each select="ownedAttribute[association]">
			<xsl:variable name="associationID" select="association/@xmi:idref"/>
			<xsl:if test="//packagedElement[@xmi:id=$associationID]/name!='realizes'">
				<xsl:apply-templates select="../../packagedElement[@xmi:id=$associationID]" mode="associationToXMI">
					<xsl:with-param name="className" select="$className"/>
				</xsl:apply-templates>
			</xsl:if>
		</xsl:for-each>
		<xsl:apply-templates select="//packagedElement[@xmi:id=$parentClass]" mode="parentClassAssociationToXMI">
			<xsl:with-param name="className" select="$className"/>
			<xsl:with-param name="overridden" select="$overridden"/>
		</xsl:apply-templates>
	</xsl:template>
	
	<xsl:template match="packagedElement" mode="parentClassToXMI">
		<xsl:param name="className"/>
		<xsl:param name="overridden"/>
		<xsl:variable name="parentClass" select="generalization/general/@xmi:idref"/>
		<xsl:apply-templates select="//packagedElement[@xmi:id=$parentClass]" mode="parentClassToXMI">
			<xsl:with-param name="className" select="$className"/>
			<xsl:with-param name="overridden">
				<xsl:apply-templates select="." mode="fillOverridden">
					<xsl:with-param name="overridden" select="$overridden"/>
				</xsl:apply-templates>
			</xsl:with-param>
		</xsl:apply-templates>
		<xsl:variable name="packageAndClassIdPrefix">
			<xsl:value-of select="$idPrefix"/>
			<xsl:value-of select="substring-before(substring-after($className,$pimIdPrefix),'-')"/>
			<xsl:text>-</xsl:text>
			<xsl:value-of select="substring-after(substring-after($className,$pimIdPrefix),'-')"/>
			<xsl:text>-</xsl:text>
		</xsl:variable>
		
		<xsl:for-each select="ownedAttribute">
			<xsl:variable name="associationID" select="association/@xmi:idref"/>
			<xsl:variable name="name">
				<xsl:choose>
					<xsl:when test="association"><xsl:value-of select="//packagedElement[@xmi:id=$associationID]/name"/></xsl:when>
					<xsl:otherwise><xsl:value-of select="name"/></xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:if test="not(contains($overridden,concat('@',$name,'@'))) and $name!='realizes'">
				<xsl:apply-templates select=".">
					<xsl:with-param name="baseid"><xsl:value-of select="$packageAndClassIdPrefix"/></xsl:with-param>
					<xsl:with-param name="name"><xsl:value-of select="$name"/></xsl:with-param>
				</xsl:apply-templates>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template match="packagedElement" mode="parentClassAssociationToXMI">
		<xsl:param name="className"/>
		<xsl:param name="overridden"/>
		<xsl:variable name="parentClass" select="generalization/general/@xmi:idref"/>
		<xsl:for-each select="ownedAttribute[association]">
			<xsl:variable name="associationID" select="association/@xmi:idref"/>
			<xsl:variable name="associationName" select="//packagedElement[@xmi:id=$associationID]/name"/>
			<xsl:if test="$associationName!='realizes' and not(contains($overridden,concat('@',$associationName ,'@')))">
				<xsl:apply-templates select="//packagedElement[@xmi:id=$associationID]" mode="associationToXMI">
					<xsl:with-param name="className" select="$className"/>
				</xsl:apply-templates>
			</xsl:if>
		</xsl:for-each>
		<xsl:apply-templates select="//packagedElement[@xmi:id=$parentClass]" mode="parentClassAssociationToXMI">
			<xsl:with-param name="className" select="$className"/>
			<xsl:with-param name="overridden">
				<xsl:apply-templates select="." mode="fillOverridden">
					<xsl:with-param name="overridden" select="$overridden"/>
				</xsl:apply-templates>
			</xsl:with-param>
		</xsl:apply-templates>
	</xsl:template>
	
	<xsl:template match="packagedElement" mode="associationToXMI">
		<xsl:param name="className"/>
		<xsl:variable name="packageAndClassIdPrefix">
			<xsl:value-of select="$idPrefix"/>
			<xsl:value-of select="substring-before(substring-after($className,$pimIdPrefix),'-')"/>
			<xsl:text>-</xsl:text>
			<xsl:value-of select="substring-after(substring-after($className,$pimIdPrefix),'-')"/>
		</xsl:variable>
		<packagedElement>
			<xsl:copy-of select="@xmi:type"/>
			<xsl:attribute name="xmi:id">
				<xsl:value-of select="$packageAndClassIdPrefix"/>
				<xsl:text>-association-</xsl:text>
				<xsl:value-of select="name"/>
			</xsl:attribute>
			<xsl:attribute name="xmi:uuid">
				<xsl:value-of select="$uuidPrefix"/>
				<xsl:text>#</xsl:text>
				<xsl:value-of select="$packageAndClassIdPrefix"/>
				<xsl:text>-association-</xsl:text>
				<xsl:value-of select="name"/>
			</xsl:attribute>
			<xsl:copy-of select="name"/>
			<xsl:copy-of select="ownedComment"/>
			<xsl:for-each select="memberEnd">
				<memberEnd>
					<xsl:choose>
						<xsl:when test="contains(@xmi:idref,'ownedEnd')">
							<xsl:attribute name="xmi:idref">
								<xsl:value-of select="$packageAndClassIdPrefix"/>
								<xsl:text>-ownedEnd-</xsl:text>
								<xsl:value-of select="../name"/>
							</xsl:attribute>
						</xsl:when>
						<xsl:otherwise>
							<xsl:attribute name="xmi:idref">
								<xsl:value-of select="$packageAndClassIdPrefix"/>
								<xsl:text>-ownedAttribute-</xsl:text>
								<xsl:value-of select="../name"/>
							</xsl:attribute>
						</xsl:otherwise>
					</xsl:choose>
				</memberEnd>
			</xsl:for-each>
			<ownedEnd>
				<xsl:copy-of select="ownedEnd/@xmi:type"/>
				<xsl:attribute name="xmi:id">
					<xsl:value-of select="$packageAndClassIdPrefix"/>
					<xsl:text>-ownedEnd-</xsl:text>
					<xsl:value-of select="name"/>
				</xsl:attribute>
				<association>
					<xsl:attribute name="xmi:idref">
						<xsl:value-of select="$packageAndClassIdPrefix"/>
						<xsl:text>-association-</xsl:text>
						<xsl:value-of select="name"/>
					</xsl:attribute>
				</association>
				<type>
					<xsl:attribute name="xmi:idref">
						<xsl:value-of select="$idPrefix"/>
						<xsl:value-of select="substring-after(ownedEnd/type/@xmi:idref,$pimIdPrefix)"/>
					</xsl:attribute>
				</type>
				<lowerValue>
					<xsl:copy-of select="ownedEnd/lowerValue/@xmi:type"/>
					<xsl:attribute name="xmi:id">
						<xsl:value-of select="$packageAndClassIdPrefix"/>
						<xsl:text>-ownedEnd-lowerValue-</xsl:text>
						<xsl:value-of select="name"/>
					</xsl:attribute>
					<xsl:copy-of select="value"/>
				</lowerValue>
				<upperValue>
					<xsl:copy-of select="ownedEnd/upperValue/@xmi:type"/>
					<xsl:attribute name="xmi:id">
						<xsl:value-of select="$packageAndClassIdPrefix"/>
						<xsl:text>-ownedEnd-upperValue-</xsl:text>
						<xsl:value-of select="name"/>
					</xsl:attribute>
					<xsl:copy-of select="ownedEnd/upperValue/value"/>
				</upperValue>
			</ownedEnd>
		</packagedElement>
	</xsl:template>
	
	<xsl:template match="packagedElement" mode="EnumAndDatatype">
		<packagedElement>
			<xsl:attribute name="xmi:id">
				<xsl:value-of select="$modelId"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="../../name"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="../name"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="name"/>
			</xsl:attribute>
			<xsl:attribute name="xmi:uuid">
				<xsl:value-of select="$uuidPrefix"/>
				<xsl:text>#</xsl:text>
				<xsl:value-of select="$modelId"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="../../name"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="../name"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="name"/>
			</xsl:attribute>
			<xsl:copy-of select="@xmi:type"/>
			<xsl:for-each select="ownedLiteral">
				<ownedLiteral>
					<xsl:attribute name="xmi:id">
						<xsl:value-of select="$modelId"/>
						<xsl:text>-</xsl:text>
						<xsl:value-of select="../../../name"/>
						<xsl:text>-</xsl:text>
						<xsl:value-of select="../../name"/>
						<xsl:text>-</xsl:text>
						<xsl:value-of select="../name"/>
						<xsl:text>-</xsl:text>
						<xsl:value-of select="name"/>
					</xsl:attribute>
					<xsl:attribute name="xmi:uuid">
						<xsl:value-of select="$uuidPrefix"/>
						<xsl:text>#</xsl:text>
						<xsl:value-of select="$modelId"/>
						<xsl:text>-</xsl:text>
						<xsl:value-of select="../../../name"/>
						<xsl:text>-</xsl:text>
						<xsl:value-of select="../../name"/>
						<xsl:text>-</xsl:text>
						<xsl:value-of select="../name"/>
						<xsl:text>-</xsl:text>
						<xsl:value-of select="name"/>
					</xsl:attribute>
					<xsl:copy-of select="@xmi:type"/>
					<xsl:copy-of select="name"></xsl:copy-of>
				</ownedLiteral>
			</xsl:for-each>
			<xsl:for-each select="ownedAttribute">
				<xsl:variable name="associationID" select="association/@xmi:idref"/>
				<xsl:variable name="name">
					<xsl:choose>
						<xsl:when test="association"><xsl:value-of select="//packagedElement[@xmi:id=$associationID]/name"/></xsl:when>
						<xsl:otherwise><xsl:value-of select="name"/></xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:if test="$name!='realizes'">
					<xsl:apply-templates select=".">
						<xsl:with-param name="baseid">
							<xsl:value-of select="$idPrefix"/>
							<xsl:value-of select="../../name"/>
							<xsl:text>-</xsl:text>
							<xsl:value-of select="../name"/>
							<xsl:text>-</xsl:text>						</xsl:with-param>
						<xsl:with-param name="name"><xsl:value-of select="$name"/></xsl:with-param>
					</xsl:apply-templates>
				</xsl:if>
			</xsl:for-each>
			<xsl:copy-of select="name"></xsl:copy-of>
		</packagedElement>
	</xsl:template>
		
	<xsl:template match="packagedElement" mode="fillOverridden">
		<xsl:param name="overridden"/>
		<xsl:param name="withGeneralization"/>
		<xsl:variable name="parentClass" select="generalization/general/@xmi:idref"/>
		<xsl:value-of select="$overridden"/>
		<xsl:for-each select="ownedAttribute">
			<xsl:text>@@</xsl:text>
			<xsl:choose>
				<xsl:when test="association">
					<xsl:variable name="paid" select="association/@xmi:idref"/>
					<xsl:value-of select="//packagedElement[@xmi:id=$paid]/name"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="name"/>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:text>@@</xsl:text>
		</xsl:for-each>
		<xsl:if test="$withGeneralization='true'">
			<xsl:apply-templates select="//packagedElement[@xmi:id=$parentClass]" mode="fillOverridden">
				<xsl:with-param name="withGeneralization"><xsl:text>true</xsl:text></xsl:with-param>
			</xsl:apply-templates>
		</xsl:if>
	</xsl:template>	
	
	<xsl:template match="ownedAttribute">
		<xsl:param name="baseid"></xsl:param>
		<xsl:param name="name"></xsl:param>
		<ownedAttribute>
			<xsl:copy-of select="@xmi:type"/>
			<xsl:attribute name="xmi:id">
				<xsl:value-of select="$baseid"/>
				<xsl:text>ownedAttribute-</xsl:text>
				<xsl:value-of select="$name"/>
			</xsl:attribute>
			<xsl:if test="association">
				<association>
					<xsl:attribute name="xmi:idref">
						<xsl:value-of select="$baseid"/>
						<xsl:text>association-</xsl:text>
						<xsl:value-of select="$name"/>
					</xsl:attribute>
				</association>
			</xsl:if>
			<xsl:copy-of select="agregation"/>
			<xsl:copy-of select="defaultValue"/>
			<xsl:copy-of select="isReadOnly"/>
			<xsl:copy-of select="ownedComment"/>
			<xsl:copy-of select="name"/>
			<type>
				<xsl:choose>
					<xsl:when test="type/@href">
						<xsl:copy-of select="type/@href"></xsl:copy-of>
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="xmi:idref">
							<xsl:value-of select="$idPrefix"/>
							<xsl:value-of select="substring-after(type/@xmi:idref,$pimIdPrefix)"/>
						</xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>
			</type>
			<xsl:if test="lowerValue">
				<lowerValue>
					<xsl:copy-of select="lowerValue/@xmi:type"/>
					<xsl:attribute name="xmi:id">
						<xsl:value-of select="$baseid"/>
						<xsl:text>lowerValue-</xsl:text>
						<xsl:value-of select="$name"/>
					</xsl:attribute>
					<xsl:copy-of select="value"/>
				</lowerValue>
			</xsl:if>
			<xsl:if test="upperValue">
				<upperValue>
					<xsl:copy-of select="upperValue/@xmi:type"/>
					<xsl:attribute name="xmi:id">
						<xsl:value-of select="$baseid"/>
						<xsl:text>upperValue-</xsl:text>
						<xsl:value-of select="$name"/>
					</xsl:attribute>
					<xsl:copy-of select="upperValue/value"/>
				</upperValue>
			</xsl:if>
		</ownedAttribute>
	</xsl:template>
</xsl:stylesheet>