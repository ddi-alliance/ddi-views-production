<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:uml="http://www.omg.org/spec/UML/20110701"
    xmlns:xmi="http://www.omg.org/spec/XMI/20110701" xmlns:ddifunc="ddi:functions"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
    xmlns:owl="http://www.w3.org/2002/07/owl#"
    exclude-result-prefixes="ddifunc uml xmi xs" version="2.0">

    <!-- imports -->
    <xsl:import href="../Util/support.xsl"/>
    
    <!-- options -->
    <xsl:output method="xml" indent="yes"/>

    <!-- base ontology IRI -->
    <xsl:param name="baseOntologyIRI">http://rdf-vocabulary.ddialliance.org/DDICoreVocabulary</xsl:param>
    <!-- default composition name -->
    <xsl:param name="compositionName">contains</xsl:param>

    <!-- ............... -->
    <!-- XMI root element -->
    <xsl:template match="xmi:XMI">
		<rdf:RDF>
			<owl:Ontology>
				<xsl:attribute name="rdf:about"><xsl:value-of select="$baseOntologyIRI"/></xsl:attribute>
				<rdfs:label xml:lang="en">DDI Core Vocabulary</rdfs:label>
				<rdfs:comment xml:lang="en">
					<xsl:text>This is the DDI Core Vocabulary, an RDF Schema vocabulary that defines foundational concepts For describing the domain of statistics.</xsl:text>
				</rdfs:comment>
			</owl:Ontology>
			<!-- UML model -->
			<xsl:apply-templates select="uml:Model//packagedElement[@xmi:id='ddi4_model']/packagedElement[@xmi:type='uml:Package']" mode="package"/>
		</rdf:RDF>
    </xsl:template>

    <!-- ............... -->
    <!-- UML package -->
    <xsl:template match="packagedElement" mode="package">
        <!-- UML class -->
        <xsl:apply-templates select="packagedElement[@xmi:type='uml:Class']" mode="class">
            <xsl:with-param name="packageName" select="ddifunc:rename(@name, 'package')"/>
        </xsl:apply-templates>
        <!-- UML enumeration -->
        <xsl:apply-templates select="packagedElement[@xmi:type='uml:Enumeration']" mode="enumeration">
            <xsl:with-param name="packageName" select="ddifunc:rename(@name, 'package')"/>
        </xsl:apply-templates>
    </xsl:template>
    <!-- ..... -->

    <!-- ............... -->
    <!-- UML class -->
    <xsl:template match="packagedElement" mode="enumeration">

        <!-- package name -->
        <xsl:param name="packageName"/>

        <!-- class name -->
        <xsl:variable name="className">
            <xsl:value-of select="ddifunc:rename(./@name, 'class')"/>
        </xsl:variable>
        
        <!-- class IRI -->
        <xsl:variable name="classIRI">
            <xsl:value-of select="$baseOntologyIRI"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$packageName"/>
            <xsl:text>#</xsl:text>
            <xsl:value-of select="$className"/>
        </xsl:variable>

		<xsl:comment>+++++++++++++++</xsl:comment>
		<xsl:comment>UML Enumeration</xsl:comment>
		<owl:Class>
			<xsl:attribute name="rdf:about"><xsl:value-of select="$classIRI"/></xsl:attribute>
			<owl:equivalentClass>
				<owl:Class>
					<owl:oneOf rdf:parseType="Collection">
						<xsl:for-each select="ownedLiteral">
							<rdf:Description rdf:about="http://foo.example/bar#V1">
								<xsl:attribute name="rdf:about">
									<xsl:value-of select="$baseOntologyIRI"/>
									<xsl:text>/</xsl:text>
									<xsl:value-of select="$className"/>
									<xsl:text>#</xsl:text>
									<xsl:value-of select="@name"/>
								</xsl:attribute>
							</rdf:Description>
						</xsl:for-each>
					</owl:oneOf>
				</owl:Class>
			</owl:equivalentClass>
		</owl:Class>
		<xsl:for-each select="ownedLiteral">
			<owl:NamedIndividual rdf:about="http://foo.example/bar#V1">
				<xsl:attribute name="rdf:about">
					<xsl:value-of select="$baseOntologyIRI"/>
					<xsl:text>/</xsl:text>
					<xsl:value-of select="$className"/>
					<xsl:text>#</xsl:text>
					<xsl:value-of select="@name"/>
				</xsl:attribute>
			</owl:NamedIndividual>
		</xsl:for-each>
    </xsl:template>
    <!-- ..... -->

    <!-- ............... -->
    <!-- UML class -->
    <xsl:template match="packagedElement" mode="class">

        <!-- package name -->
        <xsl:param name="packageName"/>

        <!-- class name -->
        <xsl:variable name="className">
            <xsl:value-of select="ddifunc:rename(./@name, 'class')"/>
        </xsl:variable>

        <!-- class IRI -->
        <xsl:variable name="classIRI">
            <xsl:value-of select="$baseOntologyIRI"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$packageName"/>
            <xsl:text>#</xsl:text>
            <xsl:value-of select="$className"/>
        </xsl:variable>

        <!-- label -->
        <xsl:variable name="label">
            <xsl:value-of select="ddifunc:uncamelize($className)"/>
            <!-- abstract class -->
            <xsl:if test="./@isAbstract = 'true'">
                <xsl:text> (Abstract)</xsl:text>
            </xsl:if>
        </xsl:variable>

        <!-- rdfs:isDefinedBy -->
        <xsl:variable name="isDefinedBy">
            <xsl:value-of select="$baseOntologyIRI"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$packageName"/>
        </xsl:variable>

		<xsl:comment>+++++++++++++++</xsl:comment>
		<xsl:comment>UML class</xsl:comment>
		<owl:Class>
			<xsl:attribute name="rdf:about"><xsl:value-of select="$classIRI"/></xsl:attribute>
			<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
			<rdfs:isDefinedBy>
				<xsl:attribute name="rdf:resource"><xsl:value-of select="$isDefinedBy"/></xsl:attribute>
			</rdfs:isDefinedBy>
			<rdfs:label xml:lang="en"><xsl:value-of select="$label"/></rdfs:label>
			<!-- UML generalization -->
			<xsl:apply-templates select="generalization[@xmi:type='uml:Generalization']"
				mode="generalization"/>
		</owl:Class>

        <!-- UML attribute -->
        <xsl:apply-templates select="ownedAttribute[@xmi:type='uml:Property'][@name and not(@association)]"
            mode="attribute">
            <xsl:with-param name="packageName" select="$packageName"/>
            <xsl:with-param name="domainClassIRI" select="$classIRI"/>
        </xsl:apply-templates>

        <!-- UML association or composition -->
        <xsl:apply-templates select="ownedAttribute[@xmi:type='uml:Property'][@association]"
            mode="association">
            <xsl:with-param name="domainClassPackageName" select="$packageName"/>
            <xsl:with-param name="domainClassIRI" select="$classIRI"/>
        </xsl:apply-templates>

        <!-- source cardinality -->
        <xsl:apply-templates select="ownedAttribute[@xmi:type='uml:Property'][@association]"
            mode="sourceCardinality"/>

        <!-- target cardinality -->
        <xsl:apply-templates select="ownedAttribute[@xmi:type='uml:Property'][@association]"
            mode="targetCardinality"/>

        <xsl:comment>+++++</xsl:comment>
    </xsl:template>
    <!-- ..... -->

    <!-- ............... -->
    <!-- UML generalization -->
    <xsl:template match="generalization" mode="generalization">

        <!-- super class ID -->
        <xsl:variable name="superClassID">
            <xsl:value-of select="./@general"/>
        </xsl:variable>

        <!-- super class name -->
        <xsl:variable name="superClassName">
            <xsl:value-of
                select="ddifunc:rename(//packagedElement[@xmi:type='uml:Class'][@xmi:id=$superClassID]/@name, 'class')"
            />
        </xsl:variable>

        <!-- super class package name -->
        <xsl:variable name="superClassPackageName">
            <xsl:value-of
                select="ddifunc:rename(//packagedElement[@xmi:type='uml:Class'][@xmi:id=$superClassID]/..[@xmi:type='uml:Package']/@name, 'package')"
            />
        </xsl:variable>

        <!-- super class IRI -->
        <xsl:variable name="superClassIRI">
            <xsl:value-of select="$baseOntologyIRI"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$superClassPackageName"/>
            <xsl:text>#</xsl:text>
            <xsl:value-of select="$superClassName"/>
        </xsl:variable>
		
		<rdfs:subClassOf>
			<xsl:attribute name="rdf:resource"><xsl:value-of select="$superClassIRI"/></xsl:attribute>
		</rdfs:subClassOf>
    </xsl:template>
    <!-- ..... -->

    <!-- ............... -->
    <!-- UML attribute -->
    <xsl:template match="ownedAttribute" mode="attribute">

        <!-- package name -->
        <xsl:param name="packageName"/>

        <!-- domain class IRI -->
        <xsl:param name="domainClassIRI"/>

        <!-- datatype property name -->
        <xsl:variable name="datatypePropertyName">
            <xsl:value-of select="ddifunc:rename(./@name, 'property')"/>
        </xsl:variable>

        <!-- datatype property IRI -->
        <xsl:variable name="datatypePropertyIRI">
            <xsl:value-of select="$baseOntologyIRI"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$packageName"/>
            <xsl:text>#</xsl:text>
            <xsl:value-of select="$datatypePropertyName"/>
        </xsl:variable>

        <!-- range class name or type -->
        <xsl:variable name="xmitype" select="lower-case(type/@xmi:type)"/>
        <xsl:variable name="rangeClassName">
            <xsl:choose>
                <xsl:when test="$xmitype = 'uml:primitivetype'">
                    <xsl:call-template name="defineType">
                        <xsl:with-param name="xmitype"
                            select="lower-case(tokenize(type/@href,'#')[last()])"/>
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test="$xmitype != ''">
                    <xsl:call-template name="defineType">
                        <xsl:with-param name="xmitype" select="$xmitype"/>
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="ddifunc:rename(./type/@xmi:idref, 'class')"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <!-- label -->
        <xsl:variable name="label">
            <xsl:value-of select="ddifunc:uncamelize($datatypePropertyName)"/>
        </xsl:variable>

        <!-- rdfs:isDefinedBy -->
        <xsl:variable name="isDefinedBy">
            <xsl:value-of select="$baseOntologyIRI"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$packageName"/>
        </xsl:variable>

		<xsl:comment>UML attribute</xsl:comment>
		<owl:DatatypeProperty>
			<xsl:attribute name="rdf:about"><xsl:value-of select="$datatypePropertyIRI"/></xsl:attribute>
			<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
			<rdfs:isDefinedBy>
				<xsl:attribute name="rdf:resource"><xsl:value-of select="$isDefinedBy"/></xsl:attribute>
			</rdfs:isDefinedBy>
			<rdfs:label xml:lang="en"><xsl:value-of select="$label"/></rdfs:label>
			<rdfs:domain>
				<xsl:attribute name="rdf:resource"><xsl:value-of select="$domainClassIRI"/></xsl:attribute>
			</rdfs:domain>
			<rdfs:range>
				<xsl:attribute name="rdf:resource">
					<xsl:choose>
						<xsl:when test="$xmitype != ''"/>
						<xsl:otherwise>
							<xsl:value-of select="$baseOntologyIRI"/>
							<xsl:text>/ComplexDataTypes/#</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:value-of select="$rangeClassName"/>
				</xsl:attribute>
			</rdfs:range>
			
		</owl:DatatypeProperty>
    </xsl:template>
    <!-- ..... -->

    <!-- ............... -->
    <!-- UML association or composition -->
    <xsl:template match="ownedAttribute" mode="association">

        <!-- domain class package name -->
        <xsl:param name="domainClassPackageName"/>

        <!-- domain class IRI -->
        <xsl:param name="domainClassIRI"/>

        <!-- ............... -->
        <!-- object property IRI -->

        <!-- association ID -->
        <xsl:variable name="associationID">
            <xsl:value-of select="./@association"/>
        </xsl:variable>

        <!-- objectProperty name -->
        <xsl:variable name="objectPropertyName">
            <xsl:choose>
                <xsl:when
                    test="//packagedElement[@xmi:type='uml:Association'][@xmi:id=$associationID]/@name">
                    <xsl:value-of
                        select="ddifunc:rename(//packagedElement[@xmi:type='uml:Association'][@xmi:id=$associationID]/@name, 'property')"
                    />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$compositionName"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <!-- object property IRI -->
        <xsl:variable name="objectPropertyIRI">
            <xsl:value-of select="$baseOntologyIRI"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$domainClassPackageName"/>
            <xsl:text>#</xsl:text>
            <xsl:value-of select="$objectPropertyName"/>
        </xsl:variable>

        <!-- ..... -->

        <!-- ............... -->
        <!-- range class IRI -->

        <!-- range class ID -->
        <xsl:variable name="rangeClassID">
            <xsl:value-of select="./type/@xmi:idref"/>
        </xsl:variable>

        <!-- range class name -->
        <xsl:variable name="rangeClassName">
            <xsl:value-of
                select="ddifunc:rename(//packagedElement[@xmi:type='uml:Class'][@xmi:id=$rangeClassID]/@name, 'class')"
            />
        </xsl:variable>

        <!-- range class package name -->
        <xsl:variable name="rangeClassPackageName">
            <xsl:value-of
                select="ddifunc:rename(//packagedElement[@xmi:type='uml:Class'][@xmi:id=$rangeClassID]/..[@xmi:type='uml:Package']/@name, 'package')"
            />
        </xsl:variable>

        <!-- range class IRI -->
        <xsl:variable name="rangeClassIRI">
            <xsl:value-of select="$baseOntologyIRI"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$rangeClassPackageName"/>
            <xsl:text>#</xsl:text>
            <xsl:value-of select="$rangeClassName"/>
        </xsl:variable>

        <!-- ..... -->

        <!-- label -->
        <xsl:variable name="label">
            <xsl:value-of select="ddifunc:uncamelize($objectPropertyName)"/>
        </xsl:variable>

        <!-- rdfs:isDefinedBy -->
        <xsl:variable name="isDefinedBy">
            <xsl:value-of select="$baseOntologyIRI"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="$domainClassPackageName"/>
        </xsl:variable>
		
		<xsl:comment>UML association or composition</xsl:comment>
		<owl:ObjectProperty>
			<xsl:attribute name="rdf:about"><xsl:value-of select="$objectPropertyIRI"/></xsl:attribute>
			<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
			<rdfs:isDefinedBy>
				<xsl:attribute name="rdf:resource"><xsl:value-of select="$isDefinedBy"/></xsl:attribute>
			</rdfs:isDefinedBy>
			<rdfs:label xml:lang="en"><xsl:value-of select="$label"/></rdfs:label>
			<rdfs:domain>
				<xsl:attribute name="rdf:resource"><xsl:value-of select="$domainClassIRI"/></xsl:attribute>
			</rdfs:domain>
			<rdfs:range>
				<xsl:attribute name="rdf:resource"><xsl:value-of select="$rangeClassIRI"/></xsl:attribute>
			</rdfs:range>
		</owl:ObjectProperty>
    </xsl:template>
    <!-- ..... -->

    <!-- ............... -->
    <!-- source cardinality -->
    <xsl:template match="ownedAttribute" mode="sourceCardinality">

        <!-- association ID -->
        <xsl:variable name="associationID">
            <xsl:value-of select="./@association"/>
        </xsl:variable>

        <!-- source cardinality only if either min or max cardinalities exist for specific association -->
        <xsl:if
            test="(//packagedElement[@xmi:type='uml:Association'][@xmi:id=$associationID]/ownedEnd/lowerValue or
                       //packagedElement[@xmi:type='uml:Association'][@xmi:id=$associationID]/ownedEnd/upperValue) and
                      (//packagedElement[@xmi:type='uml:Association'][@xmi:id=$associationID]/ownedEnd/lowerValue/@value!= '0' or
                       //packagedElement[@xmi:type='uml:Association'][@xmi:id=$associationID]/ownedEnd/upperValue/@value != '-1')">

            <!-- ............... -->
            <!-- source class IRI -->

            <!-- source class ID -->
            <xsl:variable name="sourceClassID">
                <xsl:value-of
                    select="//packagedElement[@xmi:type='uml:Association'][@xmi:id=$associationID]/ownedEnd/type/@xmi:idref"
                />
            </xsl:variable>

            <!-- source class name -->
            <xsl:variable name="sourceClassName">
                <xsl:value-of
                    select="ddifunc:rename(//packagedElement[@xmi:type='uml:Class'][@xmi:id=$sourceClassID]/@name, 'class')"
                />
            </xsl:variable>

            <!-- source class package name -->
            <xsl:variable name="sourceClassPackageName">
                <xsl:value-of
                    select="ddifunc:rename(//packagedElement[@xmi:type='uml:Class'][@xmi:id=$sourceClassID]/..[@xmi:type='uml:Package']/@name, 'package')"
                />
            </xsl:variable>

            <!-- source class IRI -->
            <xsl:variable name="sourceClassIRI">
                <xsl:value-of select="$baseOntologyIRI"/>
                <xsl:text>/</xsl:text>
                <xsl:value-of select="$sourceClassPackageName"/>
                <xsl:text>#</xsl:text>
                <xsl:value-of select="$sourceClassName"/>
            </xsl:variable>

            <!-- ..... -->

            <!-- ............... -->
            <!-- target class IRI -->

            <!-- target class ID -->
            <xsl:variable name="targetClassID">
                <xsl:value-of select="./type/@xmi:idref"/>
            </xsl:variable>

            <!-- target class name -->
            <xsl:variable name="targetClassName">
                <xsl:value-of
                    select="ddifunc:rename(//packagedElement[@xmi:type='uml:Class'][@xmi:id=$targetClassID]/@name, 'class')"
                />
            </xsl:variable>

            <!-- target class package name -->
            <xsl:variable name="targetClassPackageName">
                <xsl:value-of
                    select="ddifunc:rename(//packagedElement[@xmi:type='uml:Class'][@xmi:id=$targetClassID]/..[@xmi:type='uml:Package']/@name, 'package')"
                />
            </xsl:variable>

            <!-- target class IRI -->
            <xsl:variable name="targetClassIRI">
                <xsl:value-of select="$baseOntologyIRI"/>
                <xsl:text>/</xsl:text>
                <xsl:value-of select="$targetClassPackageName"/>
                <xsl:text>#</xsl:text>
                <xsl:value-of select="$targetClassName"/>
            </xsl:variable>

            <!-- ..... -->

            <!-- ............... -->
            <!-- object property IRI -->

            <!-- object property name -->
            <xsl:variable name="objectPropertyName">
                <xsl:choose>
                    <xsl:when
                        test="//packagedElement[@xmi:type='uml:Association'][@xmi:id=$associationID]/@name">
                        <xsl:value-of
                            select="ddifunc:rename(//packagedElement[@xmi:type='uml:Association'][@xmi:id=$associationID]/@name, 'property')"
                        />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$compositionName"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>

            <!-- object property IRI -->
            <xsl:variable name="objectPropertyIRI">
                <xsl:value-of select="$baseOntologyIRI"/>
                <xsl:text>/</xsl:text>
                <xsl:value-of select="$sourceClassPackageName"/>
                <xsl:text>#</xsl:text>
                <xsl:value-of select="$objectPropertyName"/>
            </xsl:variable>

            <!-- ..... -->

            <!-- min cardinality -->
            <xsl:variable name="minCardinality">
                <xsl:value-of
                    select="//packagedElement[@xmi:type='uml:Association'][@xmi:id=$associationID]/ownedEnd/lowerValue/@value"
                />
            </xsl:variable>

            <!-- max cardinality -->
            <xsl:variable name="maxCardinality">
                <xsl:value-of
                    select="//packagedElement[@xmi:type='uml:Association'][@xmi:id=$associationID]/ownedEnd/upperValue/@value"
                />
            </xsl:variable>
			
			<xsl:comment>source cardinality</xsl:comment>
			<rdf:Description>
				<xsl:attribute name="rdf:about"><xsl:value-of select="$targetClassIRI"/></xsl:attribute>
				<rdfs:subClassOf>
					<owl:Restriction>
						<owl:onProperty>
							<rdf:Description>
								<owl:inverseOf>
									<xsl:attribute name="rdf:resource"><xsl:value-of select="$objectPropertyIRI"/></xsl:attribute>
								</owl:inverseOf>
							</rdf:Description>
						</owl:onProperty>
						<owl:allValuesFrom>
							<xsl:attribute name="rdf:resource"><xsl:value-of select="$sourceClassIRI"/></xsl:attribute>
						</owl:allValuesFrom>	
						<xsl:if test="$minCardinality != '0' and $minCardinality != ''">
							<owl:minCardinality rdf:datatype="http://www.w3.org/2001/XMLSchema#integer">
								<xsl:value-of select="$minCardinality"/>
							</owl:minCardinality>
						</xsl:if>
						<xsl:if test="$maxCardinality != '-1' and $maxCardinality != ''">
							<owl:maxCardinality rdf:datatype="http://www.w3.org/2001/XMLSchema#integer">
								<xsl:value-of select="$maxCardinality"/>
							</owl:maxCardinality>
						</xsl:if>
					</owl:Restriction>
				</rdfs:subClassOf>
			</rdf:Description>
        </xsl:if>
    </xsl:template>
    <!-- ..... -->

    <!-- ............... -->
    <!-- target cardinality -->
    <xsl:template match="ownedAttribute" mode="targetCardinality">

        <!-- target cardinality only if either min or max cardinalities exist for specific association -->
        <xsl:if
            test="(./lowerValue or ./upperValue) and (./lowerValue/@value != '0' or ./upperValue/@value != '-1')">

            <!-- association ID -->
            <xsl:variable name="associationID">
                <xsl:value-of select="./@association"/>
            </xsl:variable>

            <!-- ............... -->
            <!-- source class IRI -->

            <!-- source class ID -->
            <xsl:variable name="sourceClassID">
                <xsl:value-of
                    select="//packagedElement[@xmi:type='uml:Association'][@xmi:id=$associationID]/ownedEnd/type/@xmi:idref"
                />
            </xsl:variable>

            <!-- source class name -->
            <xsl:variable name="sourceClassName">
                <xsl:value-of
                    select="ddifunc:rename(//packagedElement[@xmi:type='uml:Class'][@xmi:id=$sourceClassID]/@name, 'class')"
                />
            </xsl:variable>

            <!-- source class package name -->
            <xsl:variable name="sourceClassPackageName">
                <xsl:value-of
                    select="ddifunc:rename(//packagedElement[@xmi:type='uml:Class'][@xmi:id=$sourceClassID]/..[@xmi:type='uml:Package']/@name, 'package')"
                />
            </xsl:variable>

            <!-- source class IRI -->
            <xsl:variable name="sourceClassIRI">
                <xsl:value-of select="$baseOntologyIRI"/>
                <xsl:text>/</xsl:text>
                <xsl:value-of select="$sourceClassPackageName"/>
                <xsl:text>#</xsl:text>
                <xsl:value-of select="$sourceClassName"/>
            </xsl:variable>

            <!-- ..... -->

            <!-- ............... -->
            <!-- target class IRI -->

            <!-- target class ID -->
            <xsl:variable name="targetClassID">
                <xsl:value-of select="./type/@xmi:idref"/>
            </xsl:variable>

            <!-- target class name -->
            <xsl:variable name="targetClassName">
                <xsl:value-of
                    select="ddifunc:rename(//packagedElement[@xmi:type='uml:Class'][@xmi:id=$targetClassID]/@name, 'class')"
                />
            </xsl:variable>

            <!-- target class package name -->
            <xsl:variable name="targetClassPackageName">
                <xsl:value-of
                    select="ddifunc:rename(//packagedElement[@xmi:type='uml:Class'][@xmi:id=$targetClassID]/..[@xmi:type='uml:Package']/@name, 'package')"
                />
            </xsl:variable>

            <!-- target class IRI -->
            <xsl:variable name="targetClassIRI">
                <xsl:value-of select="$baseOntologyIRI"/>
                <xsl:text>/</xsl:text>
                <xsl:value-of select="$targetClassPackageName"/>
                <xsl:text>#</xsl:text>
                <xsl:value-of select="$targetClassName"/>
            </xsl:variable>

            <!-- ..... -->

            <!-- ............... -->
            <!-- object property IRI -->

            <!-- object property name -->
            <xsl:variable name="objectPropertyName">
                <xsl:choose>
                    <xsl:when
                        test="//packagedElement[@xmi:type='uml:Association'][@xmi:id=$associationID]/@name">
                        <xsl:value-of
                            select="ddifunc:rename(//packagedElement[@xmi:type='uml:Association'][@xmi:id=$associationID]/@name, 'property')"
                        />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$compositionName"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>

            <!-- object property IRI -->
            <xsl:variable name="objectPropertyIRI">
                <xsl:value-of select="$baseOntologyIRI"/>
                <xsl:text>/</xsl:text>
                <xsl:value-of select="$sourceClassPackageName"/>
                <xsl:text>#</xsl:text>
                <xsl:value-of select="$objectPropertyName"/>
            </xsl:variable>

            <!-- ..... -->

            <!-- min cardinality -->
            <xsl:variable name="minCardinality">
                <xsl:value-of select="./lowerValue/@value"/>
            </xsl:variable>

            <!-- max cardinality -->
            <xsl:variable name="maxCardinality">
                <xsl:value-of select="./upperValue/@value"/>
            </xsl:variable>

			<xsl:comment>target cardinality</xsl:comment>
			<rdf:Description>
				<xsl:attribute name="rdf:about"><xsl:value-of select="$sourceClassIRI"/></xsl:attribute>
				<rdfs:subClassOf>
					<owl:Restriction>
						<owl:onProperty>
							<xsl:attribute name="rdf:resource"><xsl:value-of select="$objectPropertyIRI"/></xsl:attribute>
						</owl:onProperty>
						<owl:allValuesFrom>
							<xsl:attribute name="rdf:resource"><xsl:value-of select="$targetClassIRI"/></xsl:attribute>
						</owl:allValuesFrom>	
						<xsl:if
							test="$minCardinality != '0' and $minCardinality != ''">
							<owl:minCardinality rdf:datatype="http://www.w3.org/2001/XMLSchema#integer">
								<xsl:value-of select="$minCardinality"/>
							</owl:minCardinality>
						</xsl:if>
						<xsl:if
							test="$maxCardinality != '-1' and $maxCardinality != ''">
							<owl:maxCardinality rdf:datatype="http://www.w3.org/2001/XMLSchema#integer">
								<xsl:value-of select="$maxCardinality"/>
							</owl:maxCardinality>
						</xsl:if>
					</owl:Restriction>
				</rdfs:subClassOf>
			</rdf:Description>
        </xsl:if>
    </xsl:template>

    <!-- define primitive types -->
    <xsl:template name="defineType">
        <xsl:param name="xmitype"/>
        <xsl:variable name="tmp">
            <xsl:choose>
                <!-- string -->
                <xsl:when test="contains($xmitype, 'string')">
                    <xsl:text>string</xsl:text>
                </xsl:when>
                <xsl:when test="contains($xmitype,  'char')">
                    <xsl:text>string</xsl:text>
                </xsl:when>

                <!-- uml unlimitednatural eq string -->
                <xsl:when test="contains($xmitype,  'unlimitednatural')">
                    <xsl:text>string</xsl:text>
                </xsl:when>

                <!-- boolean -->
                <xsl:when test="contains($xmitype,  'boolean')">
                    <xsl:text>boolean</xsl:text>
                </xsl:when>

                <!-- numeric -->
                <xsl:when test="contains($xmitype, 'integer')">
                    <xsl:text>int</xsl:text>
                </xsl:when>
                <xsl:when test="contains($xmitype, 'long')">
                    <xsl:text>long</xsl:text>
                </xsl:when>

                <!-- real -->
                <xsl:when test="contains($xmitype, 'float') or contains($xmitype, 'real')">
                    <xsl:text>decimal</xsl:text>
                </xsl:when>

                <!-- date time -->
                <xsl:when test="contains($xmitype, 'datetime')">
                    <xsl:text>dateTime</xsl:text>
                </xsl:when>

                <!-- uri -->
                <xsl:when test="contains($xmitype, 'uri')">
                    <xsl:text>anyURI</xsl:text>
                </xsl:when>

                <!-- laguage definitions -->
                <xsl:when test="contains($xmitype, 'xs:language') or contains($xmitype, 'xml:lang')">
                    <xsl:text>language</xsl:text>
                </xsl:when>

                <!-- empty and todo types -->
                <xsl:when test="$xmitype =''">
                    <xsl:text>ALERT empty type</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>TODO </xsl:text>
                    <xsl:value-of select="$xmitype"/>
                </xsl:otherwise>

                <!-- ToDo more data types -->
            </xsl:choose>
        </xsl:variable>

        <xsl:value-of select="concat('http://www.w3.org/2001/XMLSchema#', $tmp)"/>
    </xsl:template>

</xsl:stylesheet>
