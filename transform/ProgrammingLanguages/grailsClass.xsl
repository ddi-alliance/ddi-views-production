<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:uml="http://schema.omg.org/spec/UML/2.1"
    xmlns:xmi="http://schema.omg.org/spec/XMI/2.1" xmlns:ddic="Core" xmlns:ddifunc="ddi:functions"
    xmlns:xhtml="http://www.w3.org/1999/xhtml" exclude-result-prefixes="ddifunc uml xmi"
    version="2.0">
    
    <!-- imports -->
    <xsl:import href="../Util/support.xsl"/>

    <!-- options -->
    <xsl:output method="text" indent="no"/>
    
    <!-- params -->
    <xsl:param name="processedClass"/>
    <xsl:param name="packagePrefix"/>
    <xsl:param name="packageDelimiter"/>
    
    <!-- variables -->
    <xsl:variable name="versionForPlatform">1.0.0</xsl:variable>
    
    <xsl:template match="packagedElement" mode="class">
        <xsl:variable name="paid">
            <xsl:value-of select="generalization/@general"/>
        </xsl:variable>

        <xsl:text>package </xsl:text>
        <xsl:value-of select="$packagePrefix"/>
        <xsl:value-of select="$packageDelimiter"/>
        <xsl:value-of select="../@name"/>
        <xsl:text>;

/* created by LibraryBinder with platform transformation version: </xsl:text>
        <xsl:value-of select="$versionForPlatform"/>
        <xsl:text> */

import org.gesis.ComplexDataTypes.*;
</xsl:text>
        <xsl:apply-templates select="ownedAttribute[@aggregation or @association]" mode="createImport"/>
		<xsl:text>

class </xsl:text>
		<xsl:if test="@isAbstract='true'">
			<xsl:text>abstract </xsl:text>
		</xsl:if>
		<xsl:value-of select="ddifunc:cleanName(@name)"/>
        <xsl:if test="generalization">
			<xsl:text> extends </xsl:text>
			<xsl:value-of select="$packagePrefix"/>
			<xsl:value-of select="$packageDelimiter"/>
            <xsl:value-of select="$paid"/><xsl:text>bb</xsl:text>
            <xsl:value-of select="//packagedElement[@xmi:id=$paid]/@xmi:id"/>
            <xsl:value-of select="//packagedElement[@xmi:id=$paid]/../@name"/>
            <xsl:value-of select="$packageDelimiter"/>
            <xsl:value-of select="ddifunc:cleanName(//packagedElement[@xmi:id=$paid]/@name)"/>
        </xsl:if>
		<xsl:text> {
    
//    static embedded = ['subject', 'additionalSubject', 'experince', 'levelOfWork']
</xsl:text>
		<xsl:for-each select="ownedAttribute[@xmi:type='uml:Property' and (ownedEnd/upperValue/@value!='' or ownedEnd/upperValue/@value!='1')]">
			<!-- TODO -->
			dd
		</xsl:for-each>
		
		<xsl:if test="ownedAttribute[@xmi:type='uml:Property' and (ownedEnd/upperValue/@value!='' or ownedEnd/upperValue/@value!='1')]">
			<xsl:text>
    static hasMany = [</xsl:text>
			<xsl:for-each select="ownedAttribute[@xmi:type='uml:Property' and (ownedEnd/upperValue/@value!='' or ownedEnd/upperValue/@value!='1')]">
				<!-- TODO -->
				aa
			</xsl:for-each>
			<xsl:text>experince:Experience, additionalSubject:Subject];</xsl:text>
		</xsl:if>
		<xsl:text>

    static constraints = {</xsl:text>
		<xsl:for-each select="ownedAttribute[@xmi:type='uml:Property']">
			<!-- TODO -->
			ff
		</xsl:for-each>
        <!-- agreeConditions(nullable:false);
        owner(nullable:false);
        title(nullable:true, blank:true);
        name(blank:false, size:2..50);
        lastname(blank:false, size:2..50);
        //yearofbirth(blank:false, min:1900, max:1880+Calendar.get(Calendar.YEAR));
        
        highestDegree(inList:["Hochschulabschluss", "Promotion", "Habilitation"], blank:false);
        contactOtherMail(blank:true, nullable:true);
        contactPhone(nullable:true, blank:true);
        contactHomepage(url:true, blank:true, nullable:true);
        subject(nullable:false);
        additionalSubject(nullable:true);
        experince(nullable:true);
        mainFocus(blank:false, nullable:false);
        specialization(nullable:true, blank:true);
        levelOfWork(nullable:false)
        position(nullable:true, blank:true);
        institution(blank:false, nullable:false);
        department(blank:false, nullable:false);
        city(blank:false, nullable:false);
        country(inList:["Deutschland", "Österreich", "Schweiz", "Europäische Union", "Europa außerhalb der EU", "USA/Kanada", "andere Region"], blank:false);
        committeeExperience(size:0..500, blank:true, nullable:true);
        qualifications(size:0..500, blank:true, nullable:true);
        membershipsETC(size:0..500, blank:true, nullable:true);
        note(size:0..500, blank:true, nullable:true);
        comment(size:0..500, blank:true, nullable:true);
        state(nullable:false, inList:[0, 1, 2, 5]);-->
		<xsl:text>
    }
}</xsl:text>
    </xsl:template>
    
    <xsl:template match="ownedAttribute" mode="createImport">
        <xsl:choose>
            <xsl:when test="@aggregation or @association">
                <xsl:text>import </xsl:text>
                <xsl:variable name="assId" select="type/@xmi:idref"/>
                <xsl:value-of select="$packagePrefix"/>
                <xsl:value-of select="$packageDelimiter"/>
                <xsl:value-of select="//packagedElement[@xmi:id=$assId]/../@name"/>
                <xsl:value-of select="$packageDelimiter"/>
                <xsl:value-of select="ddifunc:cleanName($assId)"/>
                <xsl:text>;
</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="ownedAttribute" mode="createProperty">
        <xsl:variable name="type" select="type/@xmi:type"/>
        <xsl:variable name="typeId" select="type/@xmi:idref"/>
        <xsl:variable name="assId" select="@association"/>
        <xsl:choose>
            <xsl:when test="@aggregation or @association">
                <xsl:text>

    @XmlIDREF
    @XmlElement(name="</xsl:text>
                <xsl:value-of select="ddifunc:cleanName(//packagedElement[@xmi:id=$assId]/@name)"/>
                <xsl:text>")
    </xsl:text>
                <xsl:choose>
                    <xsl:when test="upperValue/@value = 1">
                        <xsl:text>@ManyToOne
    private </xsl:text>
                        <xsl:value-of select="ddifunc:cleanName($typeId)"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(//packagedElement[@xmi:id=$assId]/@name)"/>
                        <xsl:text>;</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>@ManyToMany
    private List&lt;</xsl:text>
                        <xsl:value-of select="ddifunc:cleanName($typeId)"/>
                        <xsl:text>&gt; </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(//packagedElement[@xmi:id=$assId]/@name)"/>
                        <xsl:text> = new ArrayList&lt;</xsl:text>
                        <xsl:value-of select="ddifunc:cleanName($typeId)"/>
                        <xsl:text>&gt;();</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:when test="/xmi:XMI/uml:Model/packagedElement/packagedElement[@xmi:id='ComplexDataTypes']/packagedElement[@xmi:id=$type]">
                <xsl:text>

    @XmlElement(name="</xsl:text>
                <xsl:value-of select="@name"/>
                <xsl:text>")
    </xsl:text>
                <xsl:choose>
                    <xsl:when test="upperValue/@value = 1">
                        <xsl:text>@OneToOne
    private </xsl:text>
                        <xsl:value-of select="ddifunc:cleanName($type)"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text>;</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>@OneToMany
    private List&lt;</xsl:text>
                        <xsl:value-of select="ddifunc:cleanName($type)"/>
                        <xsl:text>&gt; </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text> = new ArrayList&lt;</xsl:text>
                        <xsl:value-of select="ddifunc:cleanName($type)"/>
                        <xsl:text>&gt;();</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>

    @XmlElement(name="</xsl:text>
                <xsl:value-of select="@name"/>
                <xsl:text>")
    </xsl:text>
                <xsl:text>@Column
    private </xsl:text>
                <xsl:choose>
                    <xsl:when test="ends-with(type/@href,'Boolean')"><xsl:text>boolean </xsl:text></xsl:when>
                    <xsl:when test="ends-with(type/@href,'Integer')"><xsl:text>int </xsl:text></xsl:when>
                    <xsl:when test="ends-with(type/@href,'Real')"><xsl:text>double </xsl:text></xsl:when>
                    <xsl:when test="ends-with(type/@href,'UnimitedNatural')"><xsl:text>long </xsl:text></xsl:when>
                    <xsl:when test="ends-with(type/@href,'date')"><xsl:text>java.util.Date </xsl:text></xsl:when>
                    <xsl:when test="ends-with(type/@href,'decimal')"><xsl:text>long </xsl:text></xsl:when>
                    <xsl:when test="ends-with(type/@href,'anyURI')"><xsl:text>String </xsl:text></xsl:when>
                    <xsl:when test="ends-with(type/@href,'double')"><xsl:text>double </xsl:text></xsl:when>
                    <xsl:when test="ends-with(type/@href,'float')"><xsl:text>float </xsl:text></xsl:when>
                    <xsl:when test="ends-with(type/@href,'language')"><xsl:text>String </xsl:text></xsl:when>
                    <xsl:when test="ends-with(type/@href,'NMTOKEN')"><xsl:text>String </xsl:text></xsl:when>
                    <xsl:when test="ends-with(type/@href,'NMTOKENS')"><xsl:text>String </xsl:text></xsl:when>
                    <xsl:when test="ends-with(type/@href,'dateTime')"><xsl:text>java.util.Date </xsl:text></xsl:when>
                    <xsl:otherwise><xsl:text>String </xsl:text></xsl:otherwise>
                </xsl:choose>
                <xsl:text> </xsl:text>
                <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                <xsl:text>;</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>
