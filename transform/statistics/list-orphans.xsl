<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:uml="http://www.omg.org/spec/UML/20110701" 
    xmlns:xmi="http://www.omg.org/spec/XMI/20110701" xmlns:ddifunc="ddi:functions"
    xmlns:xhtml="http://www.w3.org/1999/xhtml" exclude-result-prefixes="ddifunc uml xmi"
    version="2.0">
    
    <xsl:variable name="stylesheetVersion">1.0.0</xsl:variable>
    <xsl:output method="text" indent="no"/>
    
    <xsl:template match="xmi:XMI">
        <xsl:text>This file was created by list-orphans version </xsl:text>
        <xsl:value-of select="$stylesheetVersion"/>
        <xsl:text>
package,classname,used by inherritance
</xsl:text>
        <xsl:apply-templates select="uml:Model/packagedElement/packagedElement/packagedElement[@xmi:type='uml:Enummeration']"/>
        <xsl:apply-templates select="uml:Model/packagedElement/packagedElement/packagedElement[@xmi:type='uml:Class']"/>
    </xsl:template>
    
    <xsl:template match="packagedElement">
        <xsl:variable name="myId" select="@xmi:id"/>
        <xsl:if test="not(//ownedAttribute[type/@xmi:type=$myId]) and not(//ownedAttribute[type/@xmi:idref=$myId])">
            <xsl:value-of select="../@name"/>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="@name"/>
            <xsl:text>,</xsl:text>
            <xsl:choose>
                <xsl:when test="generalization/@general">
                    <xsl:variable name="parentClass" select="generalization/@general"/>
                    <xsl:variable name="usedByInheritance">
                        <xsl:apply-templates select="//packagedElement[@xmi:id=$parentClass]" mode="checkParents"/>
                    </xsl:variable>
                    <xsl:value-of select="$usedByInheritance"/>
                </xsl:when>
                <xsl:otherwise><xsl:text>false</xsl:text></xsl:otherwise>
            </xsl:choose>
            <xsl:text>
</xsl:text>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="packagedElement" mode="checkParents">
        <xsl:variable name="myId" select="@xmi:id"/>
        <xsl:variable name="amI">
            <xsl:choose>
                <xsl:when test="not(//ownedAttribute[type/@xmi:type=$myId]) and not(//ownedAttribute[type/@xmi:idref=$myId])">false</xsl:when>
                <xsl:otherwise>true</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="contains(@name,'Identifiable')">false</xsl:when>
            <xsl:when test="generalization/@general">
                <xsl:variable name="parentClass" select="generalization/@general"/>
                <xsl:variable name="isParent">
                    <xsl:apply-templates select="//packagedElement[@xmi:id=$parentClass]" mode="checkParents"/>
                </xsl:variable>
                <xsl:value-of select="$amI or $isParent"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$amI"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
</xsl:stylesheet>