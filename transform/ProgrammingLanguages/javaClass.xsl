<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:uml="http://www.omg.org/spec/UML/20110701" 
	xmlns:xmi="http://www.omg.org/spec/XMI/20110701" xmlns:ddifunc="ddi:functions"
    xmlns:xhtml="http://www.w3.org/1999/xhtml" exclude-result-prefixes="ddifunc uml xmi"
    version="2.0">
    
    <!-- imports -->
    <xsl:import href="../Util/support.xsl"/>
    
    <!-- variables -->
    <xsl:variable name="versionForPlatform">1.0.0</xsl:variable>
    
    <xsl:template match="packagedElement" mode="class">
        <xsl:variable name="paid">
            <xsl:value-of select="generalization/@general"/>
        </xsl:variable>

        <xsl:text>package </xsl:text>
        <xsl:value-of select="$packagePrefix"/>
        <xsl:value-of select="$packageDelimiter"/>
        <xsl:value-of select="../@name"/>
        <xsl:text>;

/* created by LibraryBinder with platform transformation version: </xsl:text>
        <xsl:value-of select="$versionForPlatform"/>
        <xsl:text> */

import org.gesis.ComplexDataTypes.*;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;
import javax.xml.bind.annotation.*;
</xsl:text>
        <xsl:apply-templates select="ownedAttribute[(@aggregation or @association) and not(contains(@xmi:id, '_realizes_source'))]" mode="createImport"/>
		<xsl:text>
/*
</xsl:text>
        <xsl:value-of select="ownedComment/body"/>
		<xsl:text>
*/
@Entity
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="</xsl:text>
        <xsl:value-of select="ddifunc:cleanName(@name)"/>
        <xsl:text>")
public class </xsl:text>
		<xsl:if test="@isAbstract='1'">
			<xsl:text>abstract </xsl:text>
		</xsl:if>
		<xsl:value-of select="ddifunc:cleanName(@name)"/>
        <xsl:if test="generalization">
			<xsl:text> extends </xsl:text>
			<xsl:value-of select="$packagePrefix"/>
			<xsl:value-of select="$packageDelimiter"/>
            <xsl:value-of select="//packagedElement[@xmi:id=$paid]/../@name"/>
            <xsl:value-of select="$packageDelimiter"/>
            <xsl:value-of
                select="ddifunc:cleanName(//packagedElement[@xmi:id=$paid]/@name)"/>
        </xsl:if>
		<xsl:text> {
</xsl:text>
        <xsl:apply-templates select="ownedAttribute[not(contains(@xmi:id, '_realizes_source'))]" mode="createProperty"/>
        <xsl:apply-templates select="ownedAttribute[not(contains(@xmi:id, '_realizes_source'))]" mode="createGetterSetter"/>
        <xsl:text>
}</xsl:text>
    </xsl:template>
	
    <xsl:template match="ownedAttribute" mode="createImport">
        <xsl:choose>
            <xsl:when test="@aggregation or @association">
                <xsl:text>import </xsl:text>
                <xsl:variable name="assId" select="type/@xmi:idref"/>
                <xsl:value-of select="$packagePrefix"/>
                <xsl:value-of select="$packageDelimiter"/>
                <xsl:value-of select="//packagedElement[@xmi:id=$assId]/../@name"/>
                <xsl:value-of select="$packageDelimiter"/>
                <xsl:value-of select="ddifunc:cleanName($assId)"/>
                <xsl:text>;
</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="ownedAttribute" mode="createProperty">
        <xsl:variable name="type" select="type/@xmi:type"/>
        <xsl:variable name="typeId" select="type/@xmi:idref"/>
        <xsl:variable name="assId" select="@association"/>
		<xsl:text>

	/*
	</xsl:text>
		<xsl:value-of select="//packagedElement[@xmi:id=$assId]/ownedComment/body"/>
		<xsl:text>
	*/</xsl:text>
        <xsl:choose>
            <xsl:when test="@aggregation or @association">
                <xsl:text>
    @XmlIDREF
    @XmlElement(name="</xsl:text>
                <xsl:value-of select="ddifunc:cleanName(//packagedElement[@xmi:id=$assId]/@name)"/>
                <xsl:text>")
    </xsl:text>
                <xsl:choose>
                    <xsl:when test="upperValue/@value = 1">
                        <xsl:text>@ManyToOne
    private </xsl:text>
                        <xsl:value-of select="ddifunc:cleanName($typeId)"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(//packagedElement[@xmi:id=$assId]/@name)"/>
                        <xsl:text>;</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>@ManyToMany
    private List&lt;</xsl:text>
                        <xsl:value-of select="ddifunc:cleanName($typeId)"/>
                        <xsl:text>&gt; </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(//packagedElement[@xmi:id=$assId]/@name)"/>
                        <xsl:text> = new ArrayList&lt;</xsl:text>
                        <xsl:value-of select="ddifunc:cleanName($typeId)"/>
                        <xsl:text>&gt;();</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:when test="/xmi:XMI/uml:Model/packagedElement/packagedElement[@xmi:id='ComplexDataTypes']/packagedElement[@xmi:id=$type]">
                <xsl:text>

    @XmlElement(name="</xsl:text>
                <xsl:value-of select="@name"/>
                <xsl:text>")
    </xsl:text>
                <xsl:choose>
                    <xsl:when test="upperValue/@value = 1">
                        <xsl:text>@OneToOne
    private </xsl:text>
                        <xsl:value-of select="ddifunc:cleanName($type)"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text>;</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>@OneToMany
    private List&lt;</xsl:text>
                        <xsl:value-of select="ddifunc:cleanName($type)"/>
                        <xsl:text>&gt; </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text> = new ArrayList&lt;</xsl:text>
                        <xsl:value-of select="ddifunc:cleanName($type)"/>
                        <xsl:text>&gt;();</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>
    @XmlElement(name="</xsl:text>
                <xsl:value-of select="@name"/>
                <xsl:text>")
    </xsl:text>
                <xsl:text>@Column
    private </xsl:text>
                <xsl:choose>
                    <xsl:when test="ends-with(type/@href,'Boolean')"><xsl:text>boolean </xsl:text></xsl:when>
                    <xsl:when test="ends-with(type/@href,'Integer')"><xsl:text>int </xsl:text></xsl:when>
                    <xsl:when test="ends-with(type/@href,'Real')"><xsl:text>double </xsl:text></xsl:when>
                    <xsl:when test="ends-with(type/@href,'UnimitedNatural')"><xsl:text>long </xsl:text></xsl:when>
                    <xsl:when test="ends-with(type/@href,'date')"><xsl:text>java.util.Date </xsl:text></xsl:when>
                    <xsl:when test="ends-with(type/@href,'decimal')"><xsl:text>long </xsl:text></xsl:when>
                    <xsl:when test="ends-with(type/@href,'anyURI')"><xsl:text>String </xsl:text></xsl:when>
                    <xsl:when test="ends-with(type/@href,'double')"><xsl:text>double </xsl:text></xsl:when>
                    <xsl:when test="ends-with(type/@href,'float')"><xsl:text>float </xsl:text></xsl:when>
                    <xsl:when test="ends-with(type/@href,'language')"><xsl:text>String </xsl:text></xsl:when>
                    <xsl:when test="ends-with(type/@href,'NMTOKEN')"><xsl:text>String </xsl:text></xsl:when>
                    <xsl:when test="ends-with(type/@href,'NMTOKENS')"><xsl:text>String </xsl:text></xsl:when>
                    <xsl:when test="ends-with(type/@href,'dateTime')"><xsl:text>java.util.Date </xsl:text></xsl:when>
                    <xsl:otherwise><xsl:text>String </xsl:text></xsl:otherwise>
                </xsl:choose>
                <xsl:text> </xsl:text>
                <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                <xsl:text>;</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="ownedAttribute" mode="createGetterSetter">
        <xsl:variable name="type" select="type/@xmi:type"/>
        <xsl:variable name="typeId" select="type/@xmi:idref"/>
        <xsl:variable name="assId" select="@association"/>
        <xsl:choose>
            <xsl:when test="@aggregation or @association">
                <xsl:choose>
                    <xsl:when test="upperValue/@value = 1">
                        <xsl:text>
    
    public </xsl:text>
                        <xsl:value-of select="ddifunc:cleanName($typeId)"/>
                        <xsl:text> get</xsl:text>
                        <xsl:value-of select="ddifunc:upper-case-first(//packagedElement[@xmi:id=$assId]/@name)"/>
                        <xsl:text>() {
        return </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(//packagedElement[@xmi:id=$assId]/@name)"/>
                        <xsl:text>;
    }
    
    public void set</xsl:text>
                        <xsl:value-of select="ddifunc:upper-case-first(//packagedElement[@xmi:id=$assId]/@name)"/>
                        <xsl:text>(</xsl:text>
                        <xsl:value-of select="ddifunc:cleanName($typeId)"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(//packagedElement[@xmi:id=$assId]/@name)"/>
                        <xsl:text>) {
        this.</xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(//packagedElement[@xmi:id=$assId]/@name)"/>
                        <xsl:text> = </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(//packagedElement[@xmi:id=$assId]/@name)"/>
                        <xsl:text>;
    }</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>
    
    public List&lt;</xsl:text>
                        <xsl:value-of select="ddifunc:cleanName($typeId)"/>
                        <xsl:text>&gt; get</xsl:text>
                        <xsl:value-of select="ddifunc:upper-case-first(//packagedElement[@xmi:id=$assId]/@name)"/>
                        <xsl:text>() {
        return </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(//packagedElement[@xmi:id=$assId]/@name)"/>
                        <xsl:text>;
    }
    
    public void set</xsl:text>
                        <xsl:value-of select="ddifunc:upper-case-first(//packagedElement[@xmi:id=$assId]/@name)"/>
                        <xsl:text>(List&lt;</xsl:text>
                        <xsl:value-of select="ddifunc:cleanName($typeId)"/>
                        <xsl:text>&gt; </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(//packagedElement[@xmi:id=$assId]/@name)"/>
                        <xsl:text>) {
        this.</xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(//packagedElement[@xmi:id=$assId]/@name)"/>
                        <xsl:text> = </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(//packagedElement[@xmi:id=$assId]/@name)"/>
                        <xsl:text>;
    }
    
    public void add</xsl:text>
                        <xsl:value-of select="ddifunc:upper-case-first(//packagedElement[@xmi:id=$assId]/@name)"/>
						<xsl:text> (</xsl:text>
                        <xsl:value-of select="ddifunc:cleanName($typeId)"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first($typeId)"/>
                        <xsl:text>) {
        this.</xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(//packagedElement[@xmi:id=$assId]/@name)"/>
                        <xsl:text>.add(</xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first($typeId)"/>
                        <xsl:text>);
    }
    
    public boolean remove</xsl:text>
                        <xsl:value-of select="ddifunc:upper-case-first(//packagedElement[@xmi:id=$assId]/@name)"/>
						<xsl:text>(</xsl:text>
                        <xsl:value-of select="ddifunc:cleanName($typeId)"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first($typeId)"/>
                        <xsl:text>) {
        return this.</xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(//packagedElement[@xmi:id=$assId]/@name)"/>
                        <xsl:text>.remove(</xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first($typeId)"/>
                        <xsl:text>);
    }</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:when test="/xmi:XMI/uml:Model/packagedElement/packagedElement[@xmi:id='ExtendedPrimitives']/packagedElement[@xmi:id=$type]">
                <xsl:choose>
                    <xsl:when test="upperValue/@value = 1">
                                <xsl:text>

    public </xsl:text>
                                <xsl:value-of select="ddifunc:cleanName($type)"/>
                                <xsl:text> get</xsl:text>
                                <xsl:value-of select="ddifunc:upper-case-first(@name)"/>
                                <xsl:text>() {
        return </xsl:text>
                                <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                                <xsl:text>;
    }

    public void set</xsl:text>
                                <xsl:value-of select="ddifunc:upper-case-first(@name)"/>
                                <xsl:text>(</xsl:text>
                                <xsl:value-of select="ddifunc:cleanName($type)"/>
                                <xsl:text> </xsl:text>
                                <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                                <xsl:text>) {
        this.</xsl:text>
                                <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                                <xsl:text> = </xsl:text>
                                <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                                <xsl:text>;
    }</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                                <xsl:text>

    public List&lt;</xsl:text>
                                <xsl:value-of select="ddifunc:cleanName($type)"/>
                                <xsl:text>&gt; get</xsl:text>
                                <xsl:value-of select="ddifunc:upper-case-first(@name)"/>
                                <xsl:text>() {
        return </xsl:text>
                                <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                                <xsl:text>;
    }

    public void set</xsl:text>
                                <xsl:value-of select="ddifunc:upper-case-first(@name)"/>
                                <xsl:text>(List&lt;</xsl:text>
                                <xsl:value-of select="ddifunc:cleanName($type)"/>
                                <xsl:text>&gt; </xsl:text>
                                <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                                <xsl:text>) {
        this.</xsl:text>
                                <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                                <xsl:text> = </xsl:text>
                                <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                                <xsl:text>;
    }
                                                    
    public void add</xsl:text>
                                <xsl:value-of select="ddifunc:upper-case-first(@name)"/>
								<xsl:text> (</xsl:text>
                                <xsl:value-of select="ddifunc:cleanName($type)"/>
                                <xsl:text> </xsl:text>
                                <xsl:value-of select="ddifunc:lower-case-first($type)"/>
                                <xsl:text>) {
        this.</xsl:text>
                                <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                                <xsl:text>.add(</xsl:text>
                                <xsl:value-of select="ddifunc:lower-case-first($type)"/>
                                <xsl:text>);
    }

    public boolean remove</xsl:text>
                                <xsl:value-of select="ddifunc:upper-case-first(@name)"/>
								<xsl:text>(</xsl:text>
                                <xsl:value-of select="ddifunc:cleanName($type)"/>
                                <xsl:text> </xsl:text>
                                <xsl:value-of select="ddifunc:lower-case-first($type)"/>
                                <xsl:text>) {
        return this.</xsl:text>
                                <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                                <xsl:text>.remove(</xsl:text>
                                <xsl:value-of select="ddifunc:lower-case-first($type)"/>
                                <xsl:text>);
    }</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <xsl:choose>
                    <xsl:when test="ends-with(type/@href,'Boolean')">
                        <xsl:text>
    
    public boolean get</xsl:text>
                        <xsl:value-of select="ddifunc:upper-case-first(@name)"/>
                        <xsl:text>() {
        return </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text>;
    }
    
    public void set</xsl:text>
                        <xsl:value-of select="ddifunc:upper-case-first(@name)"/>
                        <xsl:text>(boolean </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text>) {
        this.</xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text> = </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text>;
    }</xsl:text>
                    </xsl:when>
                    <xsl:when test="ends-with(type/@href,'Integer')">
                        <xsl:text>
    
    public int get</xsl:text>
                        <xsl:value-of select="ddifunc:upper-case-first(@name)"/>
                        <xsl:text>() {
    return </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text>;
    }
    
    public void set</xsl:text>
                        <xsl:value-of select="ddifunc:upper-case-first(@name)"/>
                        <xsl:text>(int </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text>) {
    this.</xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text> = </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text>;
    }</xsl:text>
                    </xsl:when>
                    <xsl:when test="ends-with(type/@href,'Real')">
                        <xsl:text>
    
    public double get</xsl:text>
                        <xsl:value-of select="ddifunc:upper-case-first(@name)"/>
                        <xsl:text>() {
    return </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text>;
    }
    
    public void set</xsl:text>
                        <xsl:value-of select="ddifunc:upper-case-first(@name)"/>
                        <xsl:text>(double </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text>) {
    this.</xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text> = </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text>;
    }</xsl:text>
                    </xsl:when>
                    <xsl:when test="ends-with(type/@href,'UnimitedNatural')">
                        <xsl:text>
    
    public long get</xsl:text>
                        <xsl:value-of select="ddifunc:upper-case-first(@name)"/>
                        <xsl:text>() {
    return </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text>;
    }
    
    public void set</xsl:text>
                        <xsl:value-of select="ddifunc:upper-case-first(@name)"/>
                        <xsl:text>(long </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text>) {
    this.</xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text> = </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text>;
    }</xsl:text>
                    </xsl:when>
                    <xsl:when test="ends-with(type/@href,'date')">
                        <xsl:text>
    
    public java.util.Date get</xsl:text>
                        <xsl:value-of select="ddifunc:upper-case-first(@name)"/>
                        <xsl:text>() {
    return </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text>;
    }
    
    public void set</xsl:text>
                        <xsl:value-of select="ddifunc:upper-case-first(@name)"/>
                        <xsl:text>(java.util.Date </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text>) {
    this.</xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text> = </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text>;
    }</xsl:text>
                    </xsl:when>
                    <xsl:when test="ends-with(type/@href,'decimal')">
                        <xsl:text>
    
    public long get</xsl:text>
                        <xsl:value-of select="ddifunc:upper-case-first(@name)"/>
                        <xsl:text>() {
    return </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text>;
    }
    
    public void set</xsl:text>
                        <xsl:value-of select="ddifunc:upper-case-first(@name)"/>
                        <xsl:text>(long </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text>) {
    this.</xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text> = </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text>;
    }</xsl:text>
                    </xsl:when>
                    <xsl:when test="ends-with(type/@href,'double')">
                        <xsl:text>
    
    public double get</xsl:text>
                        <xsl:value-of select="ddifunc:upper-case-first(@name)"/>
                        <xsl:text>() {
    return </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text>;
    }
    
    public void set</xsl:text>
                        <xsl:value-of select="ddifunc:upper-case-first(@name)"/>
                        <xsl:text>(double </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text>) {
    this.</xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text> = </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text>;
    }</xsl:text>
                    </xsl:when>
                    <xsl:when test="ends-with(type/@href,'float')">
                        <xsl:text>
    
    public float get</xsl:text>
                        <xsl:value-of select="ddifunc:upper-case-first(@name)"/>
                        <xsl:text>() {
    return </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text>;
    }
    
    public void set</xsl:text>
                        <xsl:value-of select="ddifunc:upper-case-first(@name)"/>
                        <xsl:text>(float </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text>) {
    this.</xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text> = </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text>;
    }</xsl:text>
                    </xsl:when>
                    <xsl:when test="ends-with(type/@href,'dateTime')">
                        <xsl:text>
    
    public java.util.Date get</xsl:text>
                        <xsl:value-of select="ddifunc:upper-case-first(@name)"/>
                        <xsl:text>() {
    return </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text>;
    }
    
    public void set</xsl:text>
                        <xsl:value-of select="ddifunc:upper-case-first(@name)"/>
                        <xsl:text>(java.util.Date </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text>) {
    this.</xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text> = </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text>;
    }</xsl:text>
                    </xsl:when>
                    <!-- <xsl:when test="ends-with(type/@href,'language')">
                        <xsl:text>String </xsl:text>
                    </xsl:when>
                    <xsl:when test="ends-with(type/@href,'NMTOKEN')">
                        <xsl:text>String </xsl:text>
                    </xsl:when>
                    <xsl:when test="ends-with(type/@href,'NMTOKENS')">
                        <xsl:text>String </xsl:text>
                    </xsl:when>
                    <xsl:when test="ends-with(type/@href,'anyURI')">
                        <xsl:text>String </xsl:text>
                    </xsl:when> -->
                    <xsl:otherwise>
                        <xsl:text>
    
    public String get</xsl:text>
                        <xsl:value-of select="ddifunc:upper-case-first(@name)"/>
                        <xsl:text>() {
    return </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text>;
    }
    
    public void set</xsl:text>
                        <xsl:value-of select="ddifunc:upper-case-first(@name)"/>
                        <xsl:text>(String </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text>) {
    this.</xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text> = </xsl:text>
                        <xsl:value-of select="ddifunc:lower-case-first(@name)"/>
                        <xsl:text>;
    }</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>
