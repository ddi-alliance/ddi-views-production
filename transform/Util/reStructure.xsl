<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    <xsl:template match="body" mode="reStructure">
        <xsl:apply-templates select="node()"/>
        <xsl:text>
</xsl:text>
    </xsl:template>
    
    <xsl:template match="text()">
        <xsl:value-of select="."/>
        <xsl:text>
</xsl:text>
    </xsl:template>
    
    <xsl:template match="br">
        <xsl:text>
</xsl:text>
    </xsl:template>
    
    <xsl:template match="p">
        <xsl:value-of select="."/>
        <xsl:text>
</xsl:text>
    </xsl:template>
    
    <xsl:template match="ul">
        <xsl:apply-templates select="li" mode="unordered"/>
    </xsl:template>
    
    <xsl:template match="li" mode="unordered">
        <xsl:text>* </xsl:text>
        <xsl:value-of select="."/>
        <xsl:text>
</xsl:text>
    </xsl:template>
    
    <xsl:template match="ol">
        <xsl:apply-templates select="li" mode="ordered"/>
    </xsl:template>
    
    <xsl:template match="li" mode="ordered">
        <xsl:text># </xsl:text>
        <xsl:value-of select="."/>
        <xsl:text>
</xsl:text>
    </xsl:template>
    
    <xsl:template name="filling">
        <xsl:param name="count"/>
        <xsl:param name="char">*</xsl:param>
        <xsl:param name="linefeed"/>
        <xsl:choose>
            <xsl:when test="$count &lt; 1"></xsl:when>
            <xsl:when test="$count=1">
                <xsl:value-of select="$char"/>
                <xsl:choose>
                    <xsl:when test="$linefeed='false'"></xsl:when>
                    <xsl:otherwise>
                        <xsl:text>
</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$char"/>
                <xsl:call-template name="filling">
                    <xsl:with-param name="count" select="$count - 1"/>
                    <xsl:with-param name="char" select="$char"/>
                    <xsl:with-param name="linefeed" select="$linefeed"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template name="linefeed">
        <xsl:text>
</xsl:text>
    </xsl:template>
	
	<xsl:template name="docExtract">
		<xsl:param name="partName"/>
		<xsl:param name="text"/>
		
		<xsl:variable name="ruler">
			<xsl:call-template name="filling">
				<xsl:with-param name="count" select="string-length($partName)"/>
				<xsl:with-param name="char">=</xsl:with-param>
				<xsl:with-param name="linefeed">false</xsl:with-param>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="part1" select="substring-after($text, $partName)"/>
		<xsl:variable name="part2" select="substring-after($part1, $ruler)"/>
		<xsl:variable name="part3" select="substring-before($part2, '|||')"/>
		<xsl:value-of select="normalize-space($part3)"/>
	</xsl:template>

</xsl:stylesheet>