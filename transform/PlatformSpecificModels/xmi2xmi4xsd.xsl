<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:uml="http://www.omg.org/spec/UML/20110701" 
    xmlns:mofext="http://www.omg.org/spec/MOF/20110701"
	xmlns:xmi="http://www.omg.org/spec/XMI/20110701" xmlns:ddifunc="ddi:functions"
    exclude-result-prefixes="ddifunc"
    version="2.0">

    <!-- imports -->
    <xsl:import href="flattening.xsl"/>

    <!-- options -->
    <xsl:output method="xml" indent="yes"/>
	<xsl:variable name="pimIdPrefix">
		<xsl:text>DDI4_PIM-ClassLibrary-</xsl:text>
	</xsl:variable>
	<xsl:variable name="modelId"><xsl:text>DDI4_PSM4XSD</xsl:text></xsl:variable>
	<xsl:variable name="idPrefix">
		<xsl:value-of select="$modelId"/>
		<xsl:text>-ClassLibrary-</xsl:text>
	</xsl:variable>
	<xsl:variable name="uuidPrefix">
		<xsl:text>http://ddialliance.org/Specification/DDI-Lifecycle/4.0/DDI4_PSM4XSD.xmi</xsl:text>
	</xsl:variable>
	
	<xsl:template match="xmi:XMI">
		<xmi:XMI xmi:version="2.4.1">
			<xsl:attribute name="xmi:version">
				<xsl:value-of select="xmi:version"/>
			</xsl:attribute>
			<xsl:apply-templates select="uml:Model"/>
			<xsl:copy-of select="mofext:Tag"/>
		</xmi:XMI>
	</xsl:template>
	
	<xsl:template match="uml:Model">
		<uml:Model>
			<xsl:attribute name="xmi:id"><xsl:value-of select="$modelId"/></xsl:attribute>
			<xsl:attribute name="xmi:uuid"><xsl:value-of select="uuidPrefix"/></xsl:attribute>
			<xsl:copy-of select="@xmi:type"/>
			<name>DDI4_PSM4XSD</name>
<!--			<xsl:apply-templates select="packagedElement[1]" mode="libraryToXMI"/>
			<xsl:apply-templates select="packagedElement[2]" mode="viewsToXMI"/>
			<xsl:copy-of select="ownedComment"/>-->
			<xsl:copy-of select="*"></xsl:copy-of>
		</uml:Model>
	</xsl:template>
	
	<xsl:template match="packagedElement" mode="libraryToXMI">
		<packagedElement>
			<xsl:attribute name="xmi:id">
				<xsl:value-of select="$modelId"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="name"/>
			</xsl:attribute>
			<xsl:attribute name="xmi:uuid">
				<xsl:value-of select="$uuidPrefix"/>
				<xsl:text>#</xsl:text>
				<xsl:value-of select="$modelId"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="name"/>
			</xsl:attribute>
			<xsl:copy-of select="@xmi:type"/>
			<xsl:apply-templates select="packagedElement" mode="packageToXMI"/>
			<xsl:copy-of select="name"/>
			<xsl:copy-of select="URI"/>
			<xsl:copy-of select="ownedComment"/>
		</packagedElement>
	</xsl:template>
	
	<xsl:template match="packagedElement" mode="viewsToXMI">
		<packagedElement>
			<xsl:attribute name="xmi:id">
				<xsl:value-of select="$modelId"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="name"/>
			</xsl:attribute>
			<xsl:attribute name="xmi:uuid">
				<xsl:value-of select="$uuidPrefix"/>
				<xsl:text>#</xsl:text>
				<xsl:value-of select="$modelId"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="name"/>
			</xsl:attribute>
			<xsl:copy-of select="@xmi:type"/>
			<xsl:apply-templates select="packagedElement" mode="viewToXMI"/>
			<xsl:copy-of select="name"/>
			<xsl:copy-of select="URI"/>
			<xsl:copy-of select="ownedComment"/>
		</packagedElement>
	</xsl:template>
	
	<xsl:template match="packagedElement" mode="packageToXMI">
		<packagedElement>
			<xsl:attribute name="xmi:id">
				<xsl:value-of select="$modelId"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="../name"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="name"/>
			</xsl:attribute>
			<xsl:attribute name="xmi:uuid">
				<xsl:value-of select="$uuidPrefix"/>
				<xsl:text>#</xsl:text>
				<xsl:value-of select="$modelId"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="../name"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="name"/>
			</xsl:attribute>
			<xsl:copy-of select="@xmi:type"/>
			<xsl:apply-templates select="packagedElement[@xmi:type='uml:Class' and isAbstract='true']" mode="absractClassToXMI"/>
			<xsl:apply-templates select="packagedElement[@xmi:type='uml:Class' and not(isAbstract='true')]" mode="classToXMI"/>
			<xsl:apply-templates select="packagedElement[@xmi:type='uml:Enumeration' or @xmi:type='uml:DataType' or  @xmi:type='uml:PrimitiveType']" mode="EnumAndDatatype"/>
			<xsl:copy-of select="name"/>
			<xsl:copy-of select="URI"/>
			<ownedComment>
				<body>
					<xsl:value-of select="ownedComment/body"/>
				</body>
			</ownedComment>
		</packagedElement>
	</xsl:template>

	<xsl:template match="packagedElement" mode="viewToXMI">
		<packagedElement>
			<xsl:attribute name="xmi:id">
				<xsl:value-of select="$modelId"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="../name"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="name"/>
			</xsl:attribute>
			<xsl:attribute name="xmi:uuid">
				<xsl:value-of select="$uuidPrefix"/>
				<xsl:text>#</xsl:text>
				<xsl:value-of select="$modelId"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="../name"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="name"/>
			</xsl:attribute>
			<xsl:copy-of select="@xmi:type"/>
			<xsl:apply-templates select="elementImport" mode="importToXMI"/>
			<xsl:copy-of select="name"/>
			<xsl:copy-of select="URI"/>
			<xsl:copy-of select="ownedComment"/>
		</packagedElement>
	</xsl:template>
	
	<xsl:template match="elementImport" mode="importToXMI">
		<elementImport>
			<xsl:attribute name="xmi:id">
				<xsl:value-of select="$modelId"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="../../name"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="../name"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="position()"/>
			</xsl:attribute>
			<xsl:attribute name="xmi:uuid">
				<xsl:value-of select="$uuidPrefix"/>
				<xsl:text>#</xsl:text>
				<xsl:value-of select="$modelId"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="../../name"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="../name"/>
				<xsl:text>-</xsl:text>
				<xsl:value-of select="position()"/>
			</xsl:attribute>
			<xsl:copy-of select="@xmi:type"/>
			<importedElement>
				<xsl:attribute name="xmi:idref">
					<xsl:value-of select="$idPrefix"/>
					<xsl:value-of select="substring-after(importedElement/@xmi:idref,$pimIdPrefix)"/>
				</xsl:attribute>
			</importedElement>
		</elementImport>
	</xsl:template>

</xsl:stylesheet>