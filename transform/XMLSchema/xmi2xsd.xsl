<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:uml="http://www.omg.org/spec/UML/20110701" 
	xmlns:xmi="http://www.omg.org/spec/XMI/20110701" xmlns:ddifunc="ddi:functions"
    exclude-result-prefixes="ddifunc uml xmi" version="2.0">

    <!-- imports -->
    <xsl:import href="../Util/support.xsl"/>
    
    <!-- options -->
    <xsl:output method="xml" indent="yes"/>
	
	<!-- params -->
	<xsl:param name="filepath" select="'file:///home/oliver/DDI/output/xsd/'"/>
	
    <!-- variables -->
    <!--<xsl:variable name="properties" select="document('xsd-properties.xml')"/>-->
    <xsl:variable name="modelIdentification" select="/xmi:XMI/uml:Model/packagedElement[name='DataTypes']/packagedElement[name='ModelIdentification']"/>
	<xsl:variable name="ddiMainVersion" select="$modelIdentification/ownedAttribute[name='majorVersion']/defaultValue/value"/>
    <xsl:variable name="ddiMinorVersion" select="$modelIdentification/ownedAttribute[name='minorVersion']/defaultValue/value"/>
    <xsl:variable name="acronym" select="$modelIdentification/ownedAttribute[name='acronym']/defaultValue/value"/>
    <xsl:variable name="idPrefix" select=" 'DDICDILibrary' "/>
    <!--
	Error at line 30: "A sequence of more than one item is not allowed as the first argument of", not clear??
    <xsl:variable name="idPrefix" select="/xmi:XMI/uml:Model/name"/>
    -->
    <xsl:variable name="uuidPrefix" select="$modelIdentification/ownedAttribute[name='uri']/defaultValue/value"/>
    <xsl:variable name="xsdNamespace" select="concat( $uuidPrefix, 'XSD/' )"/>
    <xsl:variable name="XMLSchemaDataTypesPrefix" select="concat( $idPrefix, '-DataTypes-XMLSchemaDataTypes-XSD' )"/>
    <xsl:variable name="UMLPrimitivesPrefix" select=" 'http://www.omg.org/spec/UML/20110701/PrimitiveTypes.xmi#' "/>
    <xsl:variable name="specialPackages">---Enumerations---RegularExpressions---StructuredDataTypes---XMLSchemaDatatypes---Utility---NewStructuredDataTypes---</xsl:variable>
    <xsl:variable name="specialClassess">---DocumentInformation---</xsl:variable>
    <xsl:variable name="specialProperties">---agency---version---id---ofType---</xsl:variable>
    <xsl:variable name="skipedProperties">---realizes---</xsl:variable>
    <xsl:variable name="attributeProperties">---XSDlanguage---xmllang---xmlspace---</xsl:variable>
    <xsl:variable name="extendableRelations"></xsl:variable>
    <xsl:variable name="stylesheetVersion">5.2.0</xsl:variable>
    <xsl:variable name="includedPackages">
        <xsl:for-each select="//packagedElement[@xmi:type='uml:Package']">
            <xsl:text>|</xsl:text>
            <xsl:value-of select="name"/>
        </xsl:for-each>
        <xsl:text>|</xsl:text>
    </xsl:variable>

    <xsl:template match="xmi:XMI">
        <protocol>
            <xsl:comment>
				<xsl:text>This file was created by xmi2xsd version </xsl:text>
				<xsl:value-of select="$stylesheetVersion"/>
			</xsl:comment>
            <SchemaFiles>
                <xsl:apply-templates select="." mode="library"/>
            </SchemaFiles>
        </protocol>
    </xsl:template>
    
    <xsl:template match="xmi:XMI" mode="library">
        <Library>
            <xsl:attribute name="filename">
                <xsl:value-of select="$filepath"/>
                <xsl:value-of select="$acronym"/>
                <xsl:text>_</xsl:text>
                <xsl:value-of select="$ddiMainVersion"/>
                <xsl:text>-</xsl:text>
                <xsl:value-of select="$ddiMinorVersion"/>
                <xsl:text>.xsd</xsl:text>
            </xsl:attribute>
            <xsl:for-each select="//packagedElement[@xmi:type='uml:Package']">
                <Package>
                    <xsl:attribute name="name" select="name"></xsl:attribute>
                </Package>
            </xsl:for-each>
        </Library>
        
        <xsl:result-document href="{$filepath}/{$acronym}_{$ddiMainVersion}-{$ddiMinorVersion}.xsd">
            <xs:schema xmlns="http://ddialliance.org/Specification/DDI-CDI/XSD/" 
                xmlns:xs="http://www.w3.org/2001/XMLSchema" 
                xmlns:vc="http://www.w3.org/2007/XMLSchema-versioning" 
                targetNamespace="{$xsdNamespace}" 
                elementFormDefault="qualified" 
                attributeFormDefault="unqualified" vc:minVersion="1.1" version="{$ddiMainVersion}-{$ddiMinorVersion}">
                
                <xsl:comment>
        			<xsl:text>This file was created by xmi2xsd version </xsl:text>
        			<xsl:value-of select="$stylesheetVersion"/>
        		</xsl:comment>
                
                <xs:import namespace="http://www.w3.org/XML/1998/namespace" schemaLocation="xml.xsd"/>
                <xs:complexType name="DDIType">
                    <!-- documentation -->
                    <xs:annotation>
                        <xs:documentation>
                            <!--<xsl:value-of select="//*/element[@xmi:idref=$name]/properties/@documentation"/>-->
                        </xs:documentation>
                    </xs:annotation>
                    <xs:sequence minOccurs="1" maxOccurs="1">
                        <!-- <xs:element maxOccurs="1" minOccurs="1" ref="DocumentInformation"/> -->
                        <xs:choice minOccurs="1" maxOccurs="unbounded">
                            <xsl:for-each select="//packagedElement[@xmi:type='uml:Package' and not(contains($specialPackages,name))]/packagedElement[@xmi:type='uml:Class' and not(contains($specialClassess,name))]">
                                <xs:element maxOccurs="unbounded" minOccurs="0" >
                                    <xsl:attribute name="ref">
                                        <xsl:value-of select="ddifunc:cleanName(name)"/>
                                    </xsl:attribute>
                                </xs:element>
                            </xsl:for-each>
                        </xs:choice>
                    </xs:sequence>
                    <xs:attribute name="type"/>
                </xs:complexType>
                <xs:element name="DDI" type="DDIType">
                    <!-- documentation -->
                    <xs:annotation>
                        <xs:documentation>
                            <!-- TODO -->
                        </xs:documentation>
                    </xs:annotation>
                </xs:element>
                <xsl:call-template name="injectDataTypes"/>
                <xsl:apply-templates select="//packagedElement[@xmi:type='uml:Package' and contains($specialPackages,name)]"
                    mode="datatypes"/>
                <xsl:apply-templates select="//packagedElement[@xmi:type='uml:Package' and not(contains($specialPackages,name))]/packagedElement[@xmi:type='uml:Enumeration']"
                    mode="enumeration"/>
                <xsl:apply-templates select="//packagedElement[@xmi:type='uml:Package' and not(contains($specialPackages,name))]"
                    mode="package"/>
            </xs:schema>
        </xsl:result-document>
        
    </xsl:template>

    <xsl:template match="packagedElement" mode="package">
        <xsl:apply-templates select="packagedElement[@xmi:type='uml:Class']" mode="class"/>
    </xsl:template>
    
    <xsl:template match="packagedElement" mode="datatypes">
        <xsl:apply-templates select="packagedElement[@xmi:type='uml:DataType']" mode="dataType"/>
        <xsl:apply-templates select="packagedElement[@xmi:type='uml:Enumeration']" mode="enumeration"/>
        <xsl:for-each select="packagedElement[@xmi:type='uml:Class']">
        <xsl:apply-templates select="." mode="class"/>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="packagedElement" mode="enumeration">
        <xs:simpleType>
            <xsl:attribute name="name">
                <xsl:value-of select="name"/>
                <xsl:text>Type</xsl:text>
            </xsl:attribute>
            
            <xs:annotation>
                <xs:documentation>
                    <xsl:value-of select="ownedComment/body"/>
                </xs:documentation>
            </xs:annotation>
            
            <xs:restriction base="xs:NMTOKEN">
                <xsl:for-each select="ownedLiteral">
                    <xs:enumeration>
                        <xsl:attribute name="value">
                            <xsl:value-of select="name"/>
                        </xsl:attribute>
            
						<xs:annotation>
							<xs:documentation>
								<xsl:value-of select="ownedComment/body"/>
							</xs:documentation>
						</xs:annotation>
            
                    </xs:enumeration>
                </xsl:for-each>
            </xs:restriction>
        </xs:simpleType>
    </xsl:template>
    
    <xsl:template match="packagedElement" mode="dataType">
        <xsl:choose>
            <xsl:when test="ownedAttribute">
                <xsl:apply-templates select="." mode="class"/>
            </xsl:when>
            <xsl:otherwise>
                <xs:simpleType>
                    <xsl:attribute name="name">
                        <xsl:value-of select="name"/>
                        <xsl:text>Type</xsl:text>
                    </xsl:attribute>
                    <xs:restriction base="xs:string">
                        <!-- TODO: currently we dont have the right place to put the pattern in for DataType elements in XMI -->
                        <!--
                        <xsl:if test="">
                            <xs:pattern>
                                <xsl:attribute name="value">
                                    <xsl:value-of select=""/>
                                </xsl:attribute>
                            </xs:pattern>
                        </xsl:if>
                        -->
                    </xs:restriction>
                </xs:simpleType>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="packagedElement" mode="class">
        <xsl:variable name="name" select="ddifunc:cleanName(name)"/>
		<xsl:variable name="mns" select="../name"/>
        <xs:element name="{$name}" type="{$name}Type">
            <xsl:if test="isAbstract='true'">
                <xsl:attribute name="abstract">true</xsl:attribute>
            </xsl:if>
            
            <!-- documentation -->
            <xs:annotation>
                <xs:documentation>
                    <xsl:value-of select="ownedComment/body"/>
                </xs:documentation>
            </xs:annotation>
        </xs:element>

        <xs:complexType  name="{$name}Type">
            <!-- documentation -->
            <xs:annotation>
                <xs:documentation>
                    <xsl:value-of select="ownedComment/body"/>
                </xs:documentation>
            </xs:annotation>
            <xsl:choose>
                <xsl:when test="ownedAttribute[name and contains($specialProperties,name)]">
                    <xsl:choose>
                        <xsl:when test="generalization/general/@xmi:idref">
                            <xsl:variable name="superclassId" select="generalization/general/@xmi:idref"/>
                            <xsl:variable name="superclassName" select="ddifunc:cleanName(//packagedElement[@xmi:id=$superclassId]/name)"/>
                            <xs:complexContent>
                                <xs:extension>
                                    <xsl:attribute name="base"><xsl:value-of select="$superclassName"/>Type</xsl:attribute>
                                    <xs:sequence>
                                        <xsl:apply-templates select="ownedAttribute[name and contains($specialProperties,name)]"/>
                                        <xs:choice minOccurs="0" maxOccurs="unbounded">
                                            <xsl:for-each select="ownedAttribute[name and not(contains($specialProperties,name)) and not(contains($attributeProperties,name))]">
                                                <xsl:apply-templates select="."/>
                                            </xsl:for-each>
                                            <xsl:apply-templates select="ownedAttribute[not(name)]"/>
                                        </xs:choice>
                                    </xs:sequence>
                                    <xsl:apply-templates select="ownedAttribute[name and contains($attributeProperties,name)]" mode="attribute"/>
                                </xs:extension>
                            </xs:complexContent>
                        </xsl:when>
                        <xsl:otherwise>
                            <xs:sequence>
                                <xsl:apply-templates select="ownedAttribute[name and contains($specialProperties,name)]"/>
                                <xs:choice minOccurs="0" maxOccurs="unbounded">
                                    <xsl:for-each select="ownedAttribute[name and not(contains($specialProperties,name)) and not(contains($attributeProperties,name))]">
                                        <xsl:apply-templates select="."/>
                                    </xsl:for-each>
                                    <xsl:apply-templates select="ownedAttribute[not(name)]"/>
                                </xs:choice>
                            </xs:sequence>
                            <xsl:apply-templates select="ownedAttribute[name and contains($attributeProperties,name)]" mode="attribute"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:choose>
                        <xsl:when test="generalization/general/@xmi:idref">
                            <xsl:variable name="superclassId" select="generalization/general/@xmi:idref"/>
                            <xsl:variable name="superclassName" select="ddifunc:cleanName(//packagedElement[@xmi:id=$superclassId]/name)"/>
                            <xs:complexContent>
                                <xs:extension>
                                    <xsl:attribute name="base"><xsl:value-of select="$superclassName"/>Type</xsl:attribute>
                                    <xs:choice minOccurs="0" maxOccurs="unbounded">
                                        <xsl:for-each select="ownedAttribute[name and not(contains($specialProperties,name)) and not(contains($attributeProperties,name))]">
                                            <xsl:apply-templates select="."/>
                                        </xsl:for-each>
                                        <xsl:apply-templates select="ownedAttribute[not(name)]"/>
                                    </xs:choice>
                                    <xsl:apply-templates select="ownedAttribute[name and contains($attributeProperties,name)]" mode="attribute"/>
                                </xs:extension>
                            </xs:complexContent>
                        </xsl:when>
                        <xsl:otherwise>
                            <xs:choice minOccurs="0" maxOccurs="unbounded">
                                <xsl:for-each select="ownedAttribute[name and not(contains($specialProperties,name)) and not(contains($attributeProperties,name))]">
                                    <xsl:apply-templates select="."/>
                                </xsl:for-each>
                                <xsl:apply-templates select="ownedAttribute[not(name)]"/>
                            </xs:choice>
                            <xsl:apply-templates select="ownedAttribute[name and contains($attributeProperties,name)]" mode="attribute"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:otherwise>
            </xsl:choose>
        </xs:complexType>
    </xsl:template>
    
    <xsl:template match="ownedAttribute" mode="attribute">
        <xsl:variable name="name" select="name"/>
        <xsl:variable name="classname" select="../name"/>
        <xs:attribute>
                <xsl:choose>
                    <xsl:when test="lower-case(name)='xmllang' or lower-case(name)='XSDlanguage'">
                        <xsl:attribute name="ref">
                            <xsl:text>xml:lang</xsl:text>
                        </xsl:attribute>
                        <xsl:attribute name="use">
                            <xsl:choose>
                                <xsl:when test="lowerValue/value='1'"><xsl:text>required</xsl:text></xsl:when>
                                <xsl:otherwise><xsl:text>optional</xsl:text></xsl:otherwise>
                            </xsl:choose>
                        </xsl:attribute>
                        <xs:annotation>
                            <xs:documentation>
                                <xsl:value-of select="ownedComment/body"/>
                            </xs:documentation>
                        </xs:annotation>
                    </xsl:when>
                    <xsl:when test="upperValue/value='*' or upperValue/value&gt;1 or upperValue/value&lt;0">
                        <xsl:attribute name="name">
                            <xsl:value-of select="name"/>
                        </xsl:attribute>
                        <xsl:attribute name="use">
                            <xsl:choose>
                                <xsl:when test="lowerValue/value='1'"><xsl:text>required</xsl:text></xsl:when>
                                <xsl:otherwise><xsl:text>optional</xsl:text></xsl:otherwise>
                            </xsl:choose>
                        </xsl:attribute>
						<xs:annotation>
							<xs:documentation>
							    <xsl:value-of select="ownedComment/body"/>
							</xs:documentation>
						</xs:annotation>
                        <xs:simpleType>
                            <xs:list>
                                <xsl:attribute name="itemType">
                                    <!-- define xs type -->
                                    <xsl:choose>
                                        <xsl:when test="type/@xmi:idref">
                                            <xsl:call-template name="defineType">
                                                <xsl:with-param name="xmitype" select="type/@xmi:idref"/>
                                            </xsl:call-template>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:call-template name="defineType">
                                                <xsl:with-param name="xmitype" select="type/@href"/>
                                            </xsl:call-template>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:attribute>
                            </xs:list>
                        </xs:simpleType>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:attribute name="name">
                            <xsl:value-of select="name"/>
                        </xsl:attribute>
                        <xsl:attribute name="type">
                            <xsl:choose>
                                <xsl:when test="type/@xmi:idref">
                                    <xsl:call-template name="defineType">
                                        <xsl:with-param name="xmitype" select="type/@xmi:idref"/>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:call-template name="defineType">
                                        <xsl:with-param name="xmitype" select="type/@href"/>
                                    </xsl:call-template>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:attribute>
                        <xsl:attribute name="use">
                            <xsl:choose>
                                <xsl:when test="lowerValue/value='1'"><xsl:text>required</xsl:text></xsl:when>
                                <xsl:otherwise><xsl:text>optional</xsl:text></xsl:otherwise>
                            </xsl:choose>
                        </xsl:attribute>
                        <!-- TODO: No example for that in XMI -->
                        <!-- xsl:attribute name="default"><xsl:text>optional</xsl:text></xsl:attribute -->
						<xs:annotation>
							<xs:documentation>
							    <xsl:value-of select="ownedComment/body"/>
							</xs:documentation>
						</xs:annotation>
                    </xsl:otherwise>
                </xsl:choose>
            <!-- documentation -->
        </xs:attribute>
    </xsl:template>
        
    <xsl:template match="ownedAttribute">
        <xsl:variable name="classname" select="../name"/>
        <xsl:choose>
            <!-- properties to skip -->
            <!-- aggregation / association -->
            <xsl:when test="association">
                <xsl:variable name="paid" select="association/@xmi:idref"/>
                <xsl:variable name="pa" select="//packagedElement[@xmi:id=$paid]"/>
                <xsl:variable name="targetName" select="type/@xmi:idref"/>
                <xsl:if test="not(contains($skipedProperties,$pa[1]/name[1]/text()))">
                    <xs:element>
                        <xsl:attribute name="name">
                            <xsl:choose>
                                <xsl:when test="string-length($pa[1]/name[1]/text()) &gt; 0">
                                    <xsl:value-of select="ddifunc:to-upper-cc($pa[1]/name[1]/text())"/>
                                        <xsl:if test="contains($extendableRelations,concat('-',$pa[1]/name[1]/text(),'-'))">
                                            <xsl:value-of select="//packagedElement[@xmi:id=$targetName]/name"/>
                                        </xsl:if>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:text>relatesTo</xsl:text>
                                    <xsl:value-of select="//packagedElement[@xmi:id=$targetName]/name"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:attribute>
                        <xsl:choose>
                            <!-- <xsl:when test="$pa/ownedEnd/lowerValue/value!=''"> -->
                            <xsl:when test="lowerValue/value!=''">
                                <xsl:attribute name="minOccurs">
                                    <!--<xsl:value-of select="$pa/ownedEnd/lowerValue/value"/>-->
                                    <xsl:value-of select="lowerValue/value"/>
                                </xsl:attribute>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:attribute name="minOccurs">0</xsl:attribute>
                            </xsl:otherwise>
                        </xsl:choose>
                        <xsl:choose>
                            <!--<xsl:when test="$pa/ownedEnd/upperValue/value='*' or $pa/ownedEnd/upperValue/value='-1' or ownedEnd/upperValue/value='' or not(ownedEnd/upperValue/value)">-->
                            <xsl:when test="$pa/ownedEnd/upperValue/value='*' or $pa/ownedEnd/upperValue/value='-1' or ownedEnd/upperValue/value='' or not(ownedEnd/upperValue/value)">
                                <xsl:attribute name="maxOccurs">
                                    <xsl:text>unbounded</xsl:text>
                                </xsl:attribute>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:attribute name="maxOccurs">
                                    <!--<xsl:value-of select="$pa/ownedEnd/upperValue/value"/>-->
                                    <xsl:value-of select="upperValue/value"/>
                                </xsl:attribute>
                            </xsl:otherwise>
                        </xsl:choose>
                        <!-- documentation -->
                        <xs:annotation>
                            <xs:documentation>
                                <xsl:value-of select="$pa/ownedComment/body"/>
                            </xs:documentation>
                        </xs:annotation>
                        <xs:complexType>
                            <xs:simpleContent>
                                <xs:extension base="ReferenceType">
                                    <xs:attribute name="typeOfClass" use="required">
                                        <xs:simpleType>
                                            <xs:restriction base="xs:NMTOKEN">
                                                <xsl:apply-templates select="//packagedElement[@xmi:id=$targetName]" mode="fillEnumeration"/>
                                            </xs:restriction>
                                        </xs:simpleType>
                                    </xs:attribute>
                                </xs:extension>
                            </xs:simpleContent>
                        </xs:complexType>
                    </xs:element>
                </xsl:if>
            </xsl:when>
            <xsl:otherwise>
                <!-- attribute -->
                <xsl:variable name="xmiidref" select="type/@xmi:idref"/>
                <xsl:choose>
                    <xsl:when test="//packagedElement[@xmi:id=$xmiidref]/isAbstract='true'">
                        <xs:choice>
                            <!-- define min - max -->
                            <xsl:choose>
                                <xsl:when test="lowerValue/value!=''">
                                    <xsl:attribute name="minOccurs">
                                        <xsl:value-of select="lowerValue/value"/>
                                    </xsl:attribute>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:attribute name="minOccurs">0</xsl:attribute>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:choose>
                                <xsl:when test="upperValue/value='*' or upperValue/value='-1' or upperValue/value='' or not(upperValue/value)">
                                    <xsl:attribute name="maxOccurs">
                                        <xsl:text>unbounded</xsl:text>
                                    </xsl:attribute>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:attribute name="maxOccurs">
                                        <xsl:value-of select="upperValue/value"/>
                                    </xsl:attribute>
                                </xsl:otherwise>
                            </xsl:choose>
                            <!-- documentation -->
                            <xs:annotation>
                                <xs:documentation>
                                    <xsl:value-of select="ownedComment/body"/>
                                </xs:documentation>
                            </xs:annotation>
                            <xsl:apply-templates select="//packagedElement[@xmi:id=$xmiidref]" mode="fillSubstitutions"/>
                        </xs:choice>
                    </xsl:when>
                    <xsl:otherwise>
                        <xs:element>
                            <xsl:attribute name="name">
                                <xsl:value-of select="ddifunc:to-upper-cc(name)"/>
                            </xsl:attribute>
                            
                            <!-- define xs type -->
                            <xsl:choose>
                                <xsl:when test="type/@xmi:idref">
                                    <xsl:call-template name="defineType">
                                        <xsl:with-param name="xmitype"
                                            select="type/@xmi:idref"/>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:call-template name="defineType">
                                        <xsl:with-param name="xmitype" select="type/@href"/>
                                    </xsl:call-template>
                                </xsl:otherwise>
                            </xsl:choose>
                            
                            <!-- define min - max -->
                            <xsl:choose>
                                <xsl:when test="lowerValue/value!=''">
                                    <xsl:attribute name="minOccurs">
                                        <xsl:value-of select="lowerValue/value"/>
                                    </xsl:attribute>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:attribute name="minOccurs">0</xsl:attribute>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:choose>
                                <xsl:when test="upperValue/value='*' or upperValue/value='-1' or upperValue/value='' or not(upperValue/value)">
                                    <xsl:attribute name="maxOccurs">
                                        <xsl:text>unbounded</xsl:text>
                                    </xsl:attribute>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:attribute name="maxOccurs">
                                        <xsl:value-of select="upperValue/value"/>
                                    </xsl:attribute>
                                </xsl:otherwise>
                            </xsl:choose>
                            <!-- documentation -->
                            <xs:annotation>
                                <xs:documentation>
                                    <xsl:value-of select="ownedComment/body"/>
                                </xs:documentation>
                            </xs:annotation>
                        </xs:element>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="packagedElement" mode="fillSubstitutions">
        <xsl:variable name="id" select="@xmi:id"/>
        <xsl:if test="not(isAbstract='true')">
            <!-- <xs:element name="LiteralText" type="LiteralTextType"/> -->
            <xs:element>
                <xsl:attribute name="name"><xsl:value-of select="name"/></xsl:attribute>
                <xsl:attribute name="type">
                    <xsl:value-of select="name"/>
                    <xsl:text>Type</xsl:text>
                </xsl:attribute>
            </xs:element>
        </xsl:if>
        <xsl:apply-templates select="//packagedElement[generalization/general/@xmi:idref=$id]" mode="fillSubstitutions"/>
    </xsl:template>
    
    <xsl:template match="packagedElement" mode="fillEnumeration">
        <xsl:if test="not(isAbstract='true')">
            <xs:enumeration>
                <xsl:attribute name="value">
                    <xsl:value-of select="name"/>
                </xsl:attribute>
            </xs:enumeration>
        </xsl:if>
        <xsl:variable name="id" select="@xmi:id"/>
        <xsl:apply-templates select="//packagedElement[generalization/general/@xmi:idref=$id]" mode="fillEnumeration"/>
    </xsl:template>

    <xsl:template name="defineType">
        <xsl:param name="xmitype"/>
        <xsl:param name="package"></xsl:param>
        
        <xsl:attribute name="type">
            <xsl:choose>
                <!-- string -->
                <xsl:when test="$xmitype=concat($UMLPrimitivesPrefix, 'String')">
                    <xsl:text>xs:string</xsl:text>
                </xsl:when>

                <!-- uml unlimitednatural eq string. should never be used in the model -->
                <xsl:when test="$xmitype=concat($UMLPrimitivesPrefix, 'UnlimitedNatural')">
                    <xsl:text>xs:string</xsl:text>
                </xsl:when>

                <!-- boolean -->
                <xsl:when test="$xmitype=concat($UMLPrimitivesPrefix, 'Boolean')">
                    <xsl:text>xs:boolean</xsl:text>
                </xsl:when>

                <!-- integer -->
                <xsl:when test="$xmitype=concat($UMLPrimitivesPrefix, 'Integer')">
                    <xsl:text>xs:int</xsl:text>
                </xsl:when>

                <!-- real -->
                <xsl:when test="$xmitype=concat($UMLPrimitivesPrefix, 'Real')">
                    <xsl:text>xs:double</xsl:text>
                </xsl:when>
                
                <!-- XML Schema Data Types -->
                <xsl:when test="starts-with($xmitype, $XMLSchemaDataTypesPrefix)">
                    <xsl:text>xs:</xsl:text>
                    <xsl:value-of select="substring-after($xmitype, $XMLSchemaDataTypesPrefix)"/>
                </xsl:when>
                
                <xsl:when test="//packagedElement[@xmi:id=$xmitype]">
                    <xsl:value-of select="//packagedElement[@xmi:id=$xmitype]/name"/>
                    <xsl:text>Type</xsl:text>
                </xsl:when>
                
                <!-- empty and todo types -->
                <xsl:when test="$xmitype =''">
                    <xsl:text>ALERT empty type</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>ALERT TODO </xsl:text>
                    <xsl:value-of select="$xmitype"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:attribute>
    </xsl:template>
    
    <xsl:template name="injectDataTypes">
        <xs:complexType name="ReferenceType">
            <xs:annotation>
                <xs:documentation>
					Used for referencing an identified entity expressed in DDI by a URN as its node content. 
					
					The lateBound attribute has a boolean value, which - if set to true - indicates that the latest version should be used.
				</xs:documentation>
            </xs:annotation>
            <xs:simpleContent>
                <xs:extension base="xs:anyURI">
                    <xs:attribute name="isExternal" type="xs:boolean" default="false">
                        <xs:annotation>
                            <xs:documentation>Indicates that the reference is made to an external source.</xs:documentation>
                        </xs:annotation>
                    </xs:attribute>
                    <xs:attribute name="lateBound" type="xs:boolean" default="false"/>
                    <xs:attribute name="objectLanguage" type="xs:language" use="optional">
                        <xs:annotation>
                            <xs:documentation>
								Specifies the language (or language-locale pair) to use for display in references to objects which have multiple 
								languages available.
							</xs:documentation>
                        </xs:annotation>
                    </xs:attribute>   
                    <xs:attribute name="sourceContext" type="xs:anyURI" use="optional">
                        <xs:annotation>
                            <xs:documentation>
								An object may be a member of one or more collections providing specific contextual information on membership in the 
								collection and relationship to other members in the collection. If it is important to understand this context, provide 
								the URN of the relevant collection.
							</xs:documentation>
                        </xs:annotation>
                    </xs:attribute>    
                </xs:extension>
            </xs:simpleContent>
        </xs:complexType>
    </xsl:template>
</xsl:stylesheet>
