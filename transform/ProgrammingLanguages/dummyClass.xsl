<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:uml="http://schema.omg.org/spec/UML/2.1"
    xmlns:xmi="http://schema.omg.org/spec/XMI/2.1" xmlns:ddic="Core" xmlns:ddifunc="ddi:functions"
    xmlns:xhtml="http://www.w3.org/1999/xhtml" exclude-result-prefixes="ddifunc uml xmi"
    version="2.0">
    
    <!-- imports -->
    <xsl:import href="../Util/support.xsl"/>

    <!-- options -->
    <xsl:output method="text" indent="no"/>
    
    <!-- params -->
    <xsl:param name="processedClass"/>
    <xsl:param name="packagePrefix"/>
    <xsl:param name="packageDelimiter"/>
    
    <xsl:template match="xmi:XMI">
        <xsl:apply-templates select="//packagedElement[@xmi:type='uml:Class' and @name=$processedClass]" mode="createClass"/>
    </xsl:template>
    
    <xsl:template match="packagedElement" mode="createClass">
        <class>
            <xsl:attribute name="name" select="@name"/>
        </class>
    </xsl:template>
    
</xsl:stylesheet>
