<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <!-- imports -->
    <xsl:import href="javaClass.xsl"/>
    <!-- 
    available platforms for import
    <xsl:import href="javaClass.xsl"/>
    <xsl:import href="grailsClass.xsl"/>
    
    -->
    <!-- variables -->
    <xsl:variable name="properties" select="document('xsd-properties.xml')"/>
    <xsl:variable name="stylesheetVersion">1.0.0</xsl:variable>
    <!-- the filepath is mandatory to end with a / -->
<!--    <xsl:variable name="platform">grails</xsl:variable>
    <xsl:variable name="filepath">file:///c:/Work/grails/org/gesis/</xsl:variable>
    <xsl:variable name="fileending">.groovy</xsl:variable>-->
    <xsl:variable name="platform">java</xsl:variable>
    <xsl:variable name="filepath">file:///c:/Work/java/org/gesis/</xsl:variable>
    <xsl:variable name="fileending">.java</xsl:variable>
    <xsl:variable name="packagePrefix">org.gesis</xsl:variable>
    <xsl:variable name="packageDelimiter">.</xsl:variable>
    <xsl:variable name="usePackagesForFolders">true</xsl:variable>
    
</xsl:stylesheet>