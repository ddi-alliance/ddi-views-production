<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:uml="http://www.omg.org/spec/UML/20110701" 
	xmlns:xmi="http://www.omg.org/spec/XMI/20110701" xmlns:ddifunc="ddi:functions"
    xmlns:xhtml="http://www.w3.org/1999/xhtml" exclude-result-prefixes="ddifunc uml xmi"
    version="2.0">

    <!-- imports -->
    <xsl:import href="variables.xsl"/>
    
    <!-- options -->
    <xsl:output method="text" indent="no"/>
    
    <xsl:template match="xmi:XMI">
        <xsl:text>Protocol from LibraryGenerator version: </xsl:text>
        <xsl:value-of select="$stylesheetVersion"/>
        <xsl:text>
</xsl:text>
        <xsl:apply-templates select="//packagedElement[@xmi:id='ddi4_model']/packagedElement[@xmi:type='uml:Package']"
            mode="package"/>
    </xsl:template>
    
    <xsl:template match="packagedElement" mode="package">
        <xsl:value-of select="@name"/>
        <xsl:text>
</xsl:text>
        <xsl:variable name="packageName" select="@name"/>
        <xsl:for-each select="packagedElement[@xmi:type='uml:Class']">
            <xsl:variable name="filename" >
                <xsl:value-of select="$filepath"/>
                <xsl:if test="$usePackagesForFolders='true'">
                    <xsl:value-of select="$packageName"/>
                    <xsl:text>/</xsl:text>
                </xsl:if>
                <xsl:value-of select="@name"/>
                <xsl:value-of select="$fileending"/>
            </xsl:variable>
            <xsl:text>,</xsl:text>
            <xsl:value-of select="@name"/>
            <xsl:text>
</xsl:text>
            <xsl:result-document href="{$filename}">
                <xsl:apply-templates select="." mode="class"/>
            </xsl:result-document>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>